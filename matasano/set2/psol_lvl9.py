#!/usr/bin/python2
# -*- coding: utf-8 -*-

import struct

def padding(block,sizeblock):
    lenpad=-len(block)%sizeblock
    return block+struct.pack("B",lenpad)*lenpad

print padding("YELLOW SUBMARINE",20)
assert padding("YELLOW SUBMARINE",20)=="YELLOW SUBMARINE\x04\x04\x04\x04"
