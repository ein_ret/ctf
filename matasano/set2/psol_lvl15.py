#!/usr/bin/python2
# -*- coding: utf-8 -*-

from base64 import *
from Crypto.Cipher import AES
import struct
from Crypto.Util.strxor import strxor,strxor_c
import binascii,os,random


s="ICE ICE BABY\x04\x04\x04\x04"

def checkpkcs7(chaine):
    valpad=struct.unpack("B",chaine[-1])[0]
    if (chaine[-valpad:]==chaine[-1]*valpad) & ((-len(chaine[:-valpad])%16 == valpad) or (-len(chaine[:-valpad])%16 == 0)): #(-len(chaine[:-valpad])%16 == 0)) cas particulier ou padding de 16 ...
        print "Padding pkcs7 ok: %s" %(chaine[:-valpad])
        return chaine[:-valpad]
    else :
        raise ValueError("Bad padding pkcs#7")



checkpkcs7(s)
checkpkcs7("ICE ICE BABY\x01\x02\x03\x04")
