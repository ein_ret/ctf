#!/usr/bin/python2
# -*- coding: utf-8 -*-

from base64 import *
from Crypto.Cipher import AES
import struct
from Crypto.Util.strxor import strxor,strxor_c
import binascii,os,random


def aesecb_dec(cipher,key):
    aes=AES.new(key,AES.MODE_ECB)
    return aes.decrypt(padding(cipher))

def aesecb_enc(cleart,key):
    aes=AES.new(key,AES.MODE_ECB)
    return aes.encrypt(cleart)

def padding(block,sizeblock=16):
    lenpad=-len(block)%sizeblock
    return block+struct.pack("B",lenpad)*lenpad

def aescbc_enc(cleart,key,iv="\x00"*16):
    cleart=padding(cleart)
    blocks=[cleart[i:i+16] for i in xrange(0,len(cleart),16)]
    blockavant=iv
    cipher=list()
    for ele in (blocks):
        blockChiffrer=aesecb_enc(strxor(ele,blockavant),key)
        cipher.append(blockChiffrer)
        blockavant=blockChiffrer
    return "".join(cipher)

def aescbc_dec(cipher,key,iv="\x00"*16):
    blocks=[cipher[i:i+16] for i in xrange(0,len(cipher),16)]
    blockavant=iv
    cleart=list()
    for ele in (blocks):
        blockClear=strxor(aesecb_dec(ele,key),blockavant)
        cleart.append(blockClear)
        blockavant=ele
    return "".join(cleart)

keyfixed=os.urandom(16)
unknowtxt="Um9sbGluJyBpbiBteSA1LjAKV2l0aCBteSByYWctdG9wIGRvd24gc28gbXkgaGFpciBjYW4gYmxvdwpUaGUgZ2lybGllcyBvbiBzdGFuZGJ5IHdhdmluZyBqdXN0IHRvIHNheSBoaQpEaWQgeW91IHN0b3A/IE5vLCBJIGp1c3QgZHJvdmUgYnkK"

print [keyfixed,"O"]

def aesecb_enc12(cleart):
    cleart=cleart+b64decode(unknowtxt)
    aes=AES.new(keyfixed,AES.MODE_ECB)
    return aes.encrypt(padding(cleart))


#AES-128-ECB(your-string || unknown-string, random-key)

def find_blocksize():
    blocksize=0
    #add caracters till the length would be different, that means the new bloc, then len(new)- len(before)= len block ....
    for i in xrange(1,64):
        mastr="A"*i
        if len(aesecb_enc12(mastr))!=len(aesecb_enc12(mastr[:-1])):
            blocksize=len(aesecb_enc12(mastr))-len(aesecb_enc12(mastr[:-1]))
            print "taille block :%i" %blocksize
            return blocksize

def check_ecb():
    mastr="A"*64
    cipher=aesecb_enc12(mastr)
    blocks=[cipher[j:j+16] for j in xrange(0,len(cipher),16)]
    if blocks[0]==blocks[1]:
        print "check ECB mode: True"
        return True
    else :
        print "check ECB mode: False"


def bflastbyte(encryptfunction,sizeblock,textknow):
    #padding text to fill block-1
    junk="A"*(sizeblock-(len(textknow)%sizeblock)-1)
    solubf=""
    for i in xrange (0,256):
        tmp=struct.pack("B",i)
        #compute all possibilties for the last byte
        cipherbf=encryptfunction(junk+textknow+tmp)
        cipher=encryptfunction(junk)
        if cipherbf[0:len(junk+textknow+tmp)]==cipher[0:len(junk+textknow+tmp)]:
            solubf+=str(tmp)
            return solubf

def bfunknow(encryptfunction,sizeblock):
    cleart=""
    lenunknow=len(encryptfunction(""))
    while lenunknow >0:
        cleart+=str(bflastbyte(encryptfunction,sizeblock,cleart))
        lenunknow-=1
    return cleart

print bfunknow(aesecb_enc12,16)
