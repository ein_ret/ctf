#!/usr/bin/python2
# -*- coding: utf-8 -*-
from base64 import *
from Crypto.Cipher import AES
from Crypto.Util.strxor import strxor,strxor_c
import binascii,os,random,struct,re
from collections import OrderedDict


def delchar(s):
    s=re.sub('([=|&])','',s)
    return s

def padding(block,sizeblock=16):
    lenpad=-len(block)%sizeblock
    return block+struct.pack("B",lenpad)*lenpad

def strtodict(s):
    s=s.split('&')
    mondict={}
    print s
    for ele in s:
        mondict[ele.split('=')[0]]=ele.split('=')[1]
    return mondict

def dictostr(dic):
    rez=""
    for k,v in dic.items():
        rez+=k+"="+v+"&"
    return rez

def profile_for(email,uid=10,role="user"):
    email=delchar(email)
    prof=OrderedDict()
    prof['email']=email
    prof['uid']=str(uid)
    prof['role']=role
    return dictostr(prof)

key=os.urandom(16)

def encrypt_prof(s):
   aes=AES.new(key,AES.MODE_ECB)
   return aes.encrypt(padding(s))

def decrypt_prof(s):
    aes=AES.new(key,AES.MODE_ECB)
    plain=aes.decrypt(s)
    #print dictostr(plain)
    return plain

"""
the string email=at12345@fdcom&uid=10&role= is encoded on 1 blocs (16 bytes) but the total string role=user is encoded on 2 blocks (32 bytes)
'\xa6(,\x1dbU\x7f\xda5\xd1\xda8La\xc56\x03.\xbfX\x0bDRd@\x9bp\xb7\xc1\x93Y\xad'
email=at12345@fdcom&uid=10&role=

Then we encoded "admin" and i fill the bloc left with space (arbitrary choice) the \x00*13 allow to force the &uid=10&role= being on the following bloc, so |email=at12345@fdcom\x00*13|admin\x20*11|&uid=10&role=
we extract the bloc which contains admin
In [141]: encrypt_prof(profile_for('at12345@fdcom'+'\x00'*13+'admin'+'\x20'*11))[32:48]
Out[141]: 'k\xec\x9d\xc7oUu\xe5Q\x06%G\xdf\x87jx'

"""
enc1=encrypt_prof(profile_for('at12345@fdcom'))[:32]
enc2=encrypt_prof(profile_for('at12345@fdcom'+'\x00'*13+'admin'+'\x20'*11))[32:48]
enc=enc1+enc2
dec=decrypt_prof(enc)

print "enc- %s \ndec- %s" %(enc,dec)
