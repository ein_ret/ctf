#!/usr/bin/python2
# -*- coding: utf-8 -*-


from base64 import *
from Crypto.Cipher import AES
import struct
from Crypto.Util.strxor import strxor,strxor_c
import binascii,os,random

def aesecb_dec(cipher,key):
    aes=AES.new(key,AES.MODE_ECB)
    return aes.decrypt(cipher)

def aesecb_enc(cleart,key):
    aes=AES.new(key,AES.MODE_ECB)
    return aes.encrypt(cleart)

def padding(block,sizeblock=16):
    lenpad=-len(block)%sizeblock
    return block+struct.pack("B",lenpad)*lenpad

def aescbc_enc(cleart,key,iv="\x00"*16):
    blocks=[cleart[i:i+16] for i in xrange(0,len(cleart),16)]
    blockavant=iv
    cipher=list()
    for ele in (blocks):
        blockChiffrer=aesecb_enc(strxor(ele,blockavant),key)
        cipher.append(blockChiffrer)
        blockavant=blockChiffrer
    return "".join(cipher)

def aescbc_dec(cipher,key,iv="\x00"*16):
    blocks=[cipher[i:i+16] for i in xrange(0,len(cipher),16)]
    blockavant=iv
    cleart=list()
    for ele in (blocks):
        blockClear=strxor(aesecb_dec(ele,key),blockavant)
        cleart.append(blockClear)
        blockavant=ele
    return "".join(cleart)

def random_enc(plaint):
    iv=os.urandom(16)
    key=os.urandom(16)
    bitran=random.randint(0,1)
    prefix=os.urandom(random.randint(5,10))
    suffix=os.urandom(random.randint(5,10))
    plaint=padding(prefix+plaint+suffix)
    if bitran:
        print "ecb enc"
        return aesecb_enc(plaint,key)
    else :
        print "cbc enc"
        return aescbc_enc(plaint,key,iv)

def detect_random_enc():
    cleart="A"*64
    cipher=random_enc(cleart)
    cipherl=[cipher[i:i+16] for i in xrange(0,len(cipher),16)]
    if cipherl[2]==cipherl[3]:
        print "detect ecb"
    else :
        print "detect cbc"

detect_random_enc()
