#!/usr/bin/python2
# -*- coding: utf-8 -*-
from base64 import *
from Crypto.Cipher import AES
import struct
from Crypto.Util.strxor import strxor,strxor_c
import binascii,os,random,re


def delchar(s):
    s=re.sub('([=|;])','',s)
    return s

def padding(block,sizeblock=16):
    lenpad=-len(block)%sizeblock
    return block+struct.pack("B",lenpad)*lenpad

keyfixed=os.urandom(16)
iv=os.urandom(16)


print [keyfixed,"O"]

def aescbc_enc16(cleart):
    cleart="comment1=cooking%20MCs;userdata="+delchar(cleart)+";comment2=%20like%20a%20pound%20of%20bacon"
    aes=AES.new(keyfixed,AES.MODE_CBC,iv)
    return aes.encrypt(padding(cleart))


def aescbc_dec16(cipher):
    aes=AES.new(keyfixed,AES.MODE_CBC,iv)
    clear=aes.decrypt(cipher)
    print clear
    return re.search("admin=true",clear)!= None

#encryption process : cipher[i] = E(cleartext[i] xor cipher-1[i])
#decryption process : cleartext[i] = D(cipher[i]) xor cipher-1[i]
#we know that cleartext[38]= D(cipher[38]) xor cipher-1[38] = '88' then if we want '61' we just need to xor 88 to nullify and xor 61

cc=list(aescbc_enc16("aaaaaaaaaaaaaaaaXadminXtrue"))

cc[32]=struct.pack("B",struct.unpack("B",cc[32])[0]^88^59)
cc[38]=struct.pack("B",struct.unpack("B",cc[38])[0]^88^61)
cc="".join(cc)

print aescbc_dec16(cc)
