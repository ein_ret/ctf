#!/usr/bin/python2
# -*- coding: utf-8 -*-

from base64 import *
from Crypto.Cipher import AES
import struct
from Crypto.Util.strxor import strxor,strxor_c
import binascii

def aesecb_dec(cipher,key):
    aes=AES.new(key,AES.MODE_ECB)
    return aes.decrypt(cipher)

def aesecb_enc(cleart,key):
    aes=AES.new(key,AES.MODE_ECB)
    return aes.encrypt(cleart)

def padding(block,sizeblock=16):
    lenpad=-len(block)%sizeblock
    return block+struct.pack("B",lenpad)*lenpad

def aescbc_enc(cleart,key,iv="\x00"*16):
    blocks=[cleart[i:i+16] for i in xrange(0,len(cleart),16)]
    blockavant=iv
    cipher=list()
    for ele in (blocks):
        blockChiffrer=aesecb_enc(strxor(ele,blockavant),key)
        cipher.append(blockChiffrer)
        blockavant=blockChiffrer
    return "".join(cipher)

def aescbc_dec(cipher,key,iv="\x00"*16):
    blocks=[cipher[i:i+16] for i in xrange(0,len(cipher),16)]
    blockavant=iv
    cleart=list()
    for ele in (blocks):
        blockClear=strxor(aesecb_dec(ele,key),blockavant)
        cleart.append(blockClear)
        blockavant=ele
    return "".join(cleart)


with open("10.txt","r") as file10:
    cipher=b64decode(file10.read())

print aescbc_dec(cipher,"YELLOW SUBMARINE")
