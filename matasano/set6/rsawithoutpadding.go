package set6

// the non padding encryption is not cover in normal package (happily) lets reimplements it

import (
	"crypto/rsa"
	"math/big"
)

// EncryptWoPadding : Encrypt without padding
func EncryptWoPadding(key *rsa.PublicKey, message *big.Int) *big.Int {
	e := big.NewInt(int64(key.E)) //key.E is a int
	return new(big.Int).Exp(message, e, key.N)
}

// DecryptWoPadding : Decrypt without padding
func DecryptWoPadding(key *rsa.PrivateKey, cipher *big.Int) *big.Int {
	return new(big.Int).Exp(cipher, key.D, key.N)
}
