package set6

import (
	"crypto/rand"
	"crypto/rsa"
	"encoding/base64"
	"fmt"
	"math/big"
)

const (
	text46 = "VGhhdCdzIHdoeSBJIGZvdW5kIHlvdSBkb24ndCBwbGF5IGFyb3VuZCB3aXRoIHRoZSBGdW5reSBDb2xkIE1lZGluYQ=="
)

// Server46 : Define structure
type Server46 struct {
	pprivateKey *rsa.PrivateKey //as lowercase, private key is not accessible outside the Package
	PpublicKey  *rsa.PublicKey
	Cipher      *big.Int
	plain       *big.Int
}

// Newserver46 : Initialize the Server struct
func Newserver46() *Server46 {
	ppriv, _ := rsa.GenerateKey(rand.Reader, 1024)
	ppublic := &ppriv.PublicKey
	message, _ := base64.StdEncoding.DecodeString(text46) //type []byte
	pplain := new(big.Int).SetBytes(message)
	cipher := EncryptWoPadding(ppublic, pplain)

	return &Server46{ppriv, ppublic, cipher, pplain}
}

// Paritycheck : Return true if even and false odd
func (srv *Server46) paritycheck(cipher *big.Int) bool {
	pplain := DecryptWoPadding(srv.pprivateKey, cipher)
	two := big.NewInt(2)
	if sign := new(big.Int).Mod(pplain, two).Sign(); sign == 0 {
		return true
	}
	return false
}

// Decryptviaparity : main method to decrypt cipher by paritycheck
/*
Explanation, here = means congruence
C = m^e (mod n)
X = 2^e (mod n)
Lets do CX = (2m)^e (mod n) it means by multplicate the cipher with X it is the same thing than multipling m by 2
So as n is odd (VERY important to notice that) if 2m is even it means that m < n/2 so we reduced the plaintext to the value [0;n/2]
In case of 2m is odd it mean that m > n/2 so we reduced the plaintext to the value [n/2;0]

To sumup after the first iteration we have 2 possibility [0;n/2] or [n/2;n], lets iterate another time with 4m
if previous state had been [0;n/2] :
  + if 4m is even it means that 4m < n/4 so we reduced the plaintext to the value [0;n/4]
  + if 4m is odd it means that 4m > n/4 so we reduced the plaintext to the value [n/8;n/4]
If previous state had been [n/2;n]
  + if 4m is even it means that 4m < n so we reduced the plaintext to the value [n/2;3n/4]
  + if 4m is odd it means that 4m > n/2 so we reduced the plaintext to the value [3n/4;n]

As we see it is 100% dichotomic each time we reduced the scope by half, to compute the new range it is just the average of current range lower+upper /2

below and exemple with little value :
m=3 and n =17
(2m mod 17) is even we have 0<m<8
(4m mod 17) is even we have 0<m<4
(8m mod 17) is odd we have 2<m<4 here we go
*/

//Decryptviaparity : use partity to find the plaintext
func (srv *Server46) Decryptviaparity() {
	lower := big.NewInt(0)
	upper := new(big.Int).Set(srv.pprivateKey.N) //we cannot use directly srv.pprivateKey.N as it is a pointer we will modify N
	bruteforcer := new(big.Int).Set(srv.Cipher)
	two := big.NewInt(2)
	twoencrypt := EncryptWoPadding(srv.PpublicKey, two) //our factor 2 modulo n

	factor := new(big.Int)

	for i := 0; i < 1024; i++ {
		//counter = counter.Add(counter, one)
		bruteforcer = bruteforcer.Mul(bruteforcer, twoencrypt)
		bruteforcer = bruteforcer.Mod(bruteforcer, srv.PpublicKey.N)

		factor = factor.Add(lower, upper)
		factor = factor.Div(factor, two)
		/*
		   /!\ Do not write upper = factor and lower = factor ..... because as the type is *big.Int it is the pointer which is copied, that's means they point to the same value ...
		       what we need is to save the value of factor into upper or lower and not copied the pointer (memory address)
		*/
		if srv.paritycheck(bruteforcer) {
			upper = upper.Set(factor)
		} else {
			lower = lower.Set(factor)
		}
		fmt.Printf("solution : %q\n", upper.Bytes())
		//fmt.Printf("factor : %v\nparity : %v\nlower : %v\nupper : %v\nmessa : %v\nn : %v\n\n", factor, srv.paritycheck(bruteforcer), lower, upper, srv.Plain, srv.PpublicKey.N)
	}
}
