package set6

import (
	"bytes"
	"fmt"
	"testing"
)

func TestLvl47(t *testing.T) {
	fmt.Printf("Start ....\n")
	srv := Newserver47()
	resultat := srv.Bleinchen()
	if !bytes.ContainsAny(resultat, "kick it, CC") {
		t.Fatalf("Failed impossible to retreive the input string : kick it, CC")
	}
}
