package set6

// the goal of this program was to improve my skills in golang and use goroutine to increase the performance of the program.
// it was a pain to implement the algorithm http://archiv.infsec.ethz.ch/education/fs08/secsem/bleichenbacher98.pdf
// please if you see any improvement or error in code, lemme know, always need to improve myself

import (
	"context"
	"crypto/rand"
	"crypto/rsa"
	"fmt"
	"math/big"
	"sync"
)

const (
	text47 = "kick it, CC"
)

var (
	one, two, three = big.NewInt(1), big.NewInt(2), big.NewInt(3)
)

////// function helpers used in this program and make it more readable
//max: return the maximum between 2 *big.Int
func max(x, y *big.Int) *big.Int {
	if x.Cmp(y) < 0 {
		return y
	}
	return x
}

//min: return the minimum between 2 *big.Int
func min(x, y *big.Int) *big.Int {
	if x.Cmp(y) < 0 {
		return x
	}
	return y
}

// divCeil : Divide 2 big.Int and return the upper Int ex: DivCeil(5,3) will return 2
func divCeil(x, y *big.Int) *big.Int {
	tmp := new(big.Int).Div(x, y)
	tmp = tmp.Add(tmp, big.NewInt(1))
	return tmp
}

//mulmod: perform a multiplication x*y mod n
func (srv *Server47) mulmod(x, y *big.Int) *big.Int {
	tmp := new(big.Int).Mul(x, y)
	return tmp.Mod(tmp, srv.public.N)
}

//Server47 : struct for serv
type Server47 struct {
	public *rsa.PublicKey
	priv   *rsa.PrivateKey
	cipher []byte
}

type interval struct {
	a, b *big.Int
}

//Newserver47 : constructeur
func Newserver47() Server47 {
	priv, _ := rsa.GenerateKey(rand.Reader, 256)
	public := priv.PublicKey
	cipher, _ := rsa.EncryptPKCS1v15(rand.Reader, &public, []byte(text47))
	fmt.Printf("pub.N %v\npub.E %v\ncipher %v\n", priv.N, public.E, cipher)
	return Server47{&public, priv, cipher}
}

//oracle : if the rsa DecryptPKCS1v15 failed, it means that the padding is wrong
func (srv *Server47) oracle(cipher *big.Int) bool {
	ciph := cipher.Bytes()
	_, err := rsa.DecryptPKCS1v15(rand.Reader, srv.priv, ciph)
	return err == nil
}

//union: compute the union of mutiple interval and order them
func union(M []interval, m interval) []interval {
	if m.a.Cmp(m.b) > 0 {
		return M
	}

	var res []interval

	for i, mi := range M {
		if mi.b.Cmp(m.a) < 0 {
			res = append(res, mi)
		} else if m.b.Cmp(mi.a) < 0 {
			return append(append(res, m), M[i:]...) //... means that it takes all element as argument. It is called a veradic https://golang.org/ref/spec#Passing_arguments_to_..._parameters
		} else {
			m = interval{a: min(mi.a, m.a), b: max(mi.b, m.b)}
		}
	}
	return append(res, m)
}

//generator1 : goroutine which increment a counter
//We use context to end the generator properly https://rakyll.org/leakingctx/ https://golang.org/pkg/context/ and avoid a leak of goroutine
func generator1(ctx context.Context) <-chan *big.Int {
	tmp := new(big.Int).Set(one)
	out := make(chan *big.Int)
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			default:
				tmp = tmp.Add(tmp, one)
				out <- tmp
			}
		}
	}()
	return out
}

func (srv *Server47) getnxtaddone(s, c0 *big.Int) *big.Int {
	resultat := make(chan *big.Int, 1)
	var wg sync.WaitGroup
	ctx, cancel := context.WithCancel(context.Background()) // use context to allow us to cancel goroutine when value is found https://golang.org/pkg/context/
	defer cancel()                                          //Make sure it's called to release resources if it is the good path

	ite := generator1(ctx) //we pass the context to generator in order to kill it when we cancel the goroutine

	for i := 1; i <= 200; i++ { //worker pool of 200 goroutine
		wg.Add(1)
		go func(i int) {
			//defer func() { wg.Done(); fmt.Printf("defer done for job %v\n", i) }()
			defer wg.Done()
			tmp := new(big.Int).Set(s)
			for !srv.oracle(srv.mulmod(c0, EncryptWoPadding(srv.public, tmp))) {
				select {
				case <-ctx.Done():
					return // lets kill the go routine by returning
				case iteration := <-ite:
					tmp.Add(s, iteration)
				}
			}
			select {
			case <-ctx.Done():
				return // lets kill the go routine by returning
			default:
			}
			//fmt.Println("s found may quit or others still runing ???")
			resultat <- tmp //requiere to set resultat chan with a cap 1 ortherwise it is blocked here till it is write. Problem it cannot be read till it exit the goroutine. Otherwise it is possible to use Mutex and local variable to parent function
			cancel()        //we cancel the context since we find the right value => it means that signal is sent to all other goroutine to be terminated
			return
		}(i)
	}
	wg.Wait() //waiting all goroutine ended
	return <-resultat
}

//could be parallelize but i understand how it works by goroutine others functions.
func (srv *Server47) foundnexts(s, c0, a, b, r, twoB, threeB *big.Int) *big.Int {
	for ; ; r.Add(r, one) {
		minS := new(big.Int).Mul(r, srv.public.N)
		minS = divCeil(minS.Add(minS, twoB), b)

		maxS := new(big.Int).Mul(r, srv.public.N)
		maxS = divCeil(maxS.Add(maxS, threeB), a)
		for s = minS; s.Cmp(maxS) < 0; s.Add(s, one) {
			if srv.oracle(srv.mulmod(c0, EncryptWoPadding(srv.public, s))) {
				fmt.Printf("s found %v\n", s)
				return s
			}
		}
	}
}

//I fail to parallelize since the union request have to handle each interval, to do that, i would re-write the union to take a interval channel in input...
func (srv *Server47) computeinterval(M []interval, s, twoB, threeB *big.Int) []interval {
	Mi := []interval{}

	for _, m := range M {
		minR := new(big.Int).Mul(m.a, s)
		minR = divCeil(minR.Sub(minR, threeB).Add(minR, one), srv.public.N)
		maxR := new(big.Int).Mul(m.b, s)
		maxR = maxR.Sub(maxR, twoB).Div(maxR, srv.public.N)

		for r := minR; r.Cmp(maxR) <= 0; r.Add(r, one) {
			a := new(big.Int).Mul(r, srv.public.N)
			a = max(m.a, divCeil(a.Add(twoB, a), s))

			b := new(big.Int).Mul(r, srv.public.N)
			b = min(m.b, new(big.Int).Div((b.Add(threeB, b).Sub(b, one)), s))

			mi := interval{a: a, b: b}
			Mi = union(Mi, mi)

		}
	}
	return Mi
}

//Bleinchen : cca attack
func (srv *Server47) Bleinchen() []byte {
	e, c0, s := big.NewInt(int64(srv.public.E)), new(big.Int).SetBytes(srv.cipher), new(big.Int)
	k := big.NewInt(int64(srv.public.N.BitLen()))

	B := new(big.Int).Sub(k, big.NewInt(16))
	B = B.Exp(two, B, nil)

	twoB, threeB := new(big.Int).Mul(two, B), new(big.Int).Mul(three, B)
	M := []interval{{twoB, new(big.Int).Sub(threeB, one)}}
	fmt.Printf("2B %x\n3B %x\nes %v%v%v\nM  %x - %x\n", twoB, threeB, e, c0, s, M[0].a, M[0].b)

	s = srv.getnxtaddone(divCeil(srv.public.N, threeB), c0)

	fmt.Printf("s1 found %v\n", s)
	for {

		if len(M) > 1 {
			s = srv.getnxtaddone(s.Add(s, one), c0)

		} else {
			a, b := M[0].a, M[0].b
			r := new(big.Int).Mul(b, s)
			r = divCeil(r.Sub(r, twoB).Mul(r, two), srv.public.N)
			s = srv.foundnexts(s, c0, a, b, r, twoB, threeB)
		}

		M = srv.computeinterval(M, s, twoB, threeB)

		if len(M) == 1 && M[0].a.Cmp(M[0].b) == 0 {
			padded := M[0].a.Bytes()
			fmt.Printf("seriously finally get the result oO : %q \n", padded)
			return padded
			//break
		}
	}

}
