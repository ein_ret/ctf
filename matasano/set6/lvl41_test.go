package set6

import (
	"crypto/rand"
	"crypto/rsa"
	"fmt"
	"math/big"
	"testing"
)

func TestLvl41(t *testing.T) {

	pm1 := new(big.Int).SetBytes([]byte("duper secret unknown"))
	ppriv, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		t.Errorf("Fail to generate key")
	}
	pub := &ppriv.PublicKey //pprive.Public() is bugged since it return crypto.PublicKey whereas rsa.PublicKey

	cipher := EncryptWoPadding(pub, pm1)
	fmt.Printf("cipher value : %v\n", cipher)

	myrandomnumber := new(big.Int).SetBytes([]byte("123456789"))
	knowncipher := EncryptWoPadding(pub, myrandomnumber)
	knowncipher = knowncipher.Mul(cipher, knowncipher).Mod(knowncipher, pub.N)

	decrypt := DecryptWoPadding(ppriv, knowncipher)
	recoverplain := new(big.Int).ModInverse(myrandomnumber, pub.N)
	recoverplain = recoverplain.Mul(recoverplain, decrypt).Mod(recoverplain, pub.N)

	if pm1.Cmp(recoverplain) != 0 {
		t.Fatalf("expected %q, get %q\n", pm1.Bytes(), recoverplain.Bytes())
	} else {
		//spm1, _ := hex.DecodeString(pm1.Text(16))
		fmt.Printf("Result : %q\n", pm1.Bytes())
	}
	/*
		Explanation here = means congruence
		C = m^e (mod n)  // C is the unknowcipher
		X = random^e (mod n)

		we have CX = (m*random)^e (mod n)
		if we decrypt CX, we get
		(CX)^d = (m*random)^(d*e) = m*random (mod n)

		As we know random it is trivial to calculate random^(-1) such as random*random^(-1)=1 mod(n) since n is prime
		So (CX)^d * random^(-1) = m*random*random(^-1) = m (mod n)

	*/

}
