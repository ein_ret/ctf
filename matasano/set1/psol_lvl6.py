# -*- coding: utf-8 -*-
#!/usr/bin/python2


from itertools import cycle,combinations,izip_longest
from Crypto.Util.strxor import strxor,strxor_c
from binascii import hexlify
from base64 import *
import string

#bin(int(hexlify("hello"),16))
#unhexlify(hex(int('0b110100001100101011011000110110001101111',2))[2:])

cipher=""

def hammingbin(str1,str2):
    #xor strings and sum bit to bit the result
    bindef=bin(int(hexlify(str1),16)^int(hexlify(str2),16))[2:]
    return sum(map(int,bindef))

def getsizekey(ciph,k):
    #mandatory to do combination, too much hazardous result if i use only 1st and 2nd block on utilise juste le 1er et 2nd block .... i make the choose to limit the check on the first 12 blocks otherwise it take so much time to compute
    blocks=[ciph[i:i+k] for i in xrange(0,len(ciph),k)][:12]
    blockCombi=list(combinations(blocks,2))
    distan=[float(hammingbin(b[0],b[1])/float(k)) for b in blockCombi]
    return sum(distan)/len(distan)

def transpo(ciph,k):
    blocks=[ciph[i:i+k] for i in xrange(0,len(ciph),k)]
    trans=list(izip_longest(*blocks,fillvalue=0))
    return trans




def maxletter(s):
    # frequency taken from http://www.apprendre-en-ligne.net/crypto/stat/anglais.html i add ' '
    besten=0
    englishLetterFreq = {'E':0.1256, 'T': 0.0915, 'A': 0.0808, 'O':0.0747, 'I':0.0724, 'N':0.0738, 'S': 0.0659, 'H':0.0527, 'R': 0.0642, 'D': 0.0399, 'L': 0.0404, 'C':0.0318, 'U': 0.0279, 'M': 0.0260, 'W': 0.0189, 'F': 0.0217, 'G': 0.0180, 'Y': 0.0165, 'P': 0.0191, 'B': 0.0167, 'V': 0.01, 'K': 0.0063, 'J': 0.0014, 'X': 0.0021, 'Q': 0.009, 'Z': 0.007,' ':0.05}
    for lettre in s:
        if string.upper(lettre) in englishLetterFreq.keys():
            besten+= englishLetterFreq[string.upper(lettre)]
    return besten


def singlexordech(s):
    rezFin=list()
    for i in xrange (256):
        rez=[strxor_c(x,i) for x in s]
        rezFin.append([maxletter("".join(rez)),i,"".join(rez)])
    return sorted(rezFin,key=lambda x:x[0],reverse=True)[0]

def getkey(trans):
    rtmp=["".join(map(str,tupl)) for tupl in trans]
    key=list()
    for element in rtmp:
        indicateur,cleN,cleart=singlexordech(element)
        #indicateur,cleN,cleart=sorted(keypart,key=lambda x:float(x[0]),reverse=True)[0]
        #print "indicateur :"+str(indicateur)+" cleN :"+chr(cleN)+" cleartext :"+cleart
        key.append(chr(cleN))
    return "".join(key)

def clexordech(s,k):
    zipcc=zip(s,cycle(k))
    rez="".join([strxor(x,y) for x,y in zipcc])
    return rez

with open("6.txt","r") as file6:
    cipher=b64decode(file6.read())

longkey=sorted(xrange(2,40),key=lambda k:getsizekey(cipher,k))[0]
print "\n====== cypher xor with key ======\n"
print "+ longueur cle probable :"+ str(longkey)


trans=transpo(cipher,longkey)
keyF= getkey(trans)
print "+ cle :"+keyF+"\n\n===== cleartext ======\n\n"
cleartext=clexordech(cipher,keyF)
print cleartext
