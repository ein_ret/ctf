#!/usr/bin/python2

from itertools import *
import binascii
from Crypto.Util.strxor import strxor_c


h="1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
h=binascii.unhexlify(h)
rezFin=list()


def maxletter(s):
        lettres=filter(lambda x:x.isalpha(),s)
        return float(len(lettres)) / float(len(s))

for i in xrange (256):
        rez=[strxor_c(x,i) for x in h]
        rezFin.append([maxletter("".join(rez)),"".join(rez)])

print max(rezFin)[1]
