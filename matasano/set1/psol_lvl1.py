#!/usr/bin/python2

import base64, binascii

strh16="49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d"
rez=base64.b64encode(binascii.unhexlify(strh16))
print(rez)
assert rez == 'SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t'
