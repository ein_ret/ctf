#!/usr/bin/python2
# -*- coding: utf-8 -*-
from base64 import *
from binascii import unhexlify,hexlify
from itertools import combinations

cipher=list()

with open("8.txt","r") as file7:
    cipher=file7.readlines()

def identecb(s):
    rez={}
    for line in s :
        line=line[:-1]
        rline=unhexlify(line)
        rez[line]=0
        blocks=[rline[i:i+16] for i in xrange(0,len(rline)-16,16)]
        combi=list(combinations(blocks,2))

        for elts in combi:
            if elts[0]==elts[1]:
                rez[line]+=1
    for k,v in rez.items():
        if v==max(rez.values()):
            return k

print identecb(cipher)
