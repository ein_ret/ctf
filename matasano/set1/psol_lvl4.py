#!/usr/bin/python2

from itertools import *

import binascii
#from Crypto.Util.strxor import strxor_c

h="1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736"
h=binascii.unhexlify(h)
rez=list()

def maxletter(s):
    lettres=filter(lambda x:x.isalpha(),s)
    try :
        return float(len(lettres)) / float(len(s))
    except ZeroDivisionError:
        return 0.0

def dechi(s):
    rezFin=list()
    for i in xrange (256):
        rtmp=[chr(ord(x)^i) for x in s]
        rezFin.append([maxletter("".join(rtmp)),"".join(rtmp)])
    return rezFin


with open("4.txt","r") as file4:
    for line in file4:
        rez.append(dechi(binascii.unhexlify(line[:-1])))

for i in rez:
    for j in filter(lambda x:x[0]>=0.8,i):
        if j is not None:
            print j
