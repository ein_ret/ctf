# -*- coding: utf-8 -*-
#!/usr/bin/python2

from base64 import *
from Crypto.Cipher import AES

cipher=""

with open("7.txt","r") as file7:
    cipher=b64decode(file7.read())

aes=AES.new("YELLOW SUBMARINE",AES.MODE_ECB)
print aes.decrypt(cipher)
