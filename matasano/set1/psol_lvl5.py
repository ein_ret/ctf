#!/usr/bin/python2


from itertools import cycle
from Crypto.Util.strxor import strxor
from binascii import hexlify


cleart="""Burning 'em, if you ain't quick and nimble
I go crazy when I hear a cymbal"""

cle="ICE"

zipcc=zip(cleart,cycle(cle))

rez=hexlify("".join([strxor(x,y) for x,y in zipcc]))
print rez
assert rez=="0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f"
