#!/usr/bin/python3
# -*- coding: utf-8 -*-

from Crypto.Random import random
from Crypto.Hash import SHA256
from Crypto.Cipher import AES
import socketserver,pickle
from base64 import b64encode,b64decode

class MyTCPHandler(socketserver.StreamRequestHandler):
    def __init__(self, request, client_address, server):
        socketserver.StreamRequestHandler.__init__(self, request, client_address, server)
        self.g=0
        self.p=0
        self.b=0
        self.clientA=0
        self.B=0
        self.secret=0

    def towrite (self,data):
        return b64encode(pickle.dumps(data))

    def padding(block,sizeblock=16):
        lenpad=-len(block)%sizeblock
        return block+bytes([lenpad]*lenpad)

    def dechif(self,message,iv):
        aes=AES.new(self.secret,AES.MODE_CBC,iv)
        dechi=aes.decrypt(message)
        dechi=dechi[:16-dechi[-1]]
        print ("dechiffre : {}\n".format(dechi))
        return dechi


    def generateB (self):
        self.b=random.randint(0,self.p)
        print ("random b :{}\n".format(self.b))
        self.B=pow(self.g,self.b,self.p)
        print ("generateB : {}\n".format(self.B))

    def handle(self):
        # self.rfile is a file-like object created by the handler;
        # we can now use e.g. readline() instead of raw recv() calls

        #recup les p,g et A
        self.data = self.rfile.readline().strip()
        self.p,self.g,self.clientA=pickle.loads(b64decode(self.data))
        print ("{} attaquant wrote: {}\n".format(self.client_address[0],self.clientA))
        # Likewise, self.wfile is a file-like object used to write back
        # to the client

        #we generate B and send secret
        self.generateB()
        self.secret=SHA256.new(str(pow(self.clientA,self.b,self.p)).encode("utf-8")).hexdigest()[:16]
        print ("secret : {}\n".format(self.secret))
        self.wfile.write(self.towrite((self.B)))

        #owe get back encrypted msg
        self.data = self.rfile.readline().strip()
        enc,iv=pickle.loads(b64decode(self.data))
        dechi=self.dechif(enc,iv)


if __name__ == "__main__":
    HOST, PORT = "localhost", 9999

    # Create the server, binding to localhost on port 9999
    server = socketserver.TCPServer((HOST, PORT), MyTCPHandler)

    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    server.serve_forever()
