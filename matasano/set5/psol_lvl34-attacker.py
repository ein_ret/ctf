#!/usr/bin/python3
# -*- coding: utf-8 -*-
from Crypto.Random import random
from Crypto.Hash import SHA256
from Crypto.Cipher import AES
import socketserver,pickle,socket
from base64 import b64encode,b64decode

class MyTCPHandler(socketserver.StreamRequestHandler):
    def __init__(self, request, client_address, server):
        socketserver.StreamRequestHandler.__init__(self, request, client_address, server)
        self.g=0
        self.p=0
        self.b=0
        self.clientA=0
        self.serverB=0
        self.secret=0

    def towrite (self,data):
        return b64encode(pickle.dumps(data))

    def padding(block,sizeblock=16):
        lenpad=-len(block)%sizeblock
        return block+bytes([lenpad]*lenpad)

    def dechif(self,message,iv):
        aes=AES.new(self.secret,AES.MODE_CBC,iv)
        dechi=aes.decrypt(message)
        dechi=dechi[:16-dechi[-1]]
        return dechi

    def toread (self,data):
        return pickle.loads(b64decode(data))

    def handle(self):
        # self.rfile is a file-like object created by the handler;
        # we can now use e.g. readline() instead of raw recv() calls

        #we get the p,g et A from the client
        self.data = self.rfile.readline().strip()
        self.p,self.g,self.clientA=pickle.loads(b64decode(self.data))
        #print ("{} wrote: {}\n".format(self.client_address[0],self.clientA))
        # Likewise, self.wfile is a file-like object used to write back
        # to the client

        #we send p,g,p to server
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try :
            sock.connect(("localhost", 9999))
            sock.sendall(self.towrite([self.p,self.g,self.p]) + b"\n")
            #we get from serverB
            received = sock.recv(1024)
            self.serverB=self.toread(received)

            print("clientA : {}\nserverB : {}".format(self.clientA, self.serverB))
            self.wfile.write(self.towrite((self.p)))
            #the secret is 0 indeed p^x mod(p) =0
            self.secret=SHA256.new("0".encode("utf-8")).hexdigest()[:16]

            #read encrypted data from client
            self.data = self.rfile.readline().strip()
            enc,iv=pickle.loads(b64decode(self.data))
            dechi=self.dechif(enc,iv)

            print ("MITM key-fixing attack on Diffie-Hellman with parameter injection == secret : {}".format(dechi))
            #we forward to the server
            sock.sendall(self.data)

        finally:
            sock.close()

if __name__ == "__main__":
    HOST, PORT = "localhost", 4242

    # Create the server, binding to localhost on port 9999
    server = socketserver.TCPServer((HOST, PORT), MyTCPHandler)

    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    server.serve_forever()
