#!/usr/bin/python3
# -*- coding: utf-8 -*-
from Crypto.Random import random
from Crypto.Hash import SHA256
from Crypto.Cipher import AES
import socketserver,pickle,socket,sys,os
from base64 import b64decode, b64encode

class Myclient():
    def __init__(self,host,port):
        self.host, self.port = host, port
        self.p=0xffffffffffffffffc90fdaa22168c234c4c6628b80dc1cd129024e088a67cc74020bbea63b139b22514a08798e3404ddef9519b3cd3a431b302b0a6df25f14374fe1356d6d51c245e485b576625e7ec6f44c42e9a637ed6b0bff5cb6f406b7edee386bfb5a899fa5ae9f24117c4b1fe649286651ece45b3dc2007cb8a163bf0598da48361c55d39a69163fa8fd24cf5f83655d23dca3ad961c62f356208552bb9ed529077096966d670c354e4abc9804f1746c08ca237327ffffffffffffffff
        self.g=2
        self.a=random.randint(0,self.p)
        self.A=pow(self.g,self.a,self.p)
        self.serverB=0
        self.secret=0

    def towrite (self,data):
        return b64encode(pickle.dumps(data))

    def toread (self,data):
        return pickle.loads(b64decode(data))

    def padding(self,block,sizeblock=16):
        lenpad=-len(block)%sizeblock
        return block+bytes([lenpad]*lenpad).decode() #in python 2.7 string == bytes en 3 it is different need to convert bytes in str via decode()

    def encmsg(self,message):
        self.secret=SHA256.new(str(pow(self.serverB,self.a,self.p)).encode("utf-8")).hexdigest()[:16]
        print ("secret : {}".format(self.secret))
        iv=os.urandom(16)
        aes=AES.new(self.secret,AES.MODE_CBC,iv)
        enc=aes.encrypt(self.padding(message))
        print ("message :{} enc :{}\n".format(message,enc))
        return [enc,iv]

    def communication(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            # Connect to server and send data
            sock.connect((self.host, self.port))
            sock.sendall(self.towrite([self.p,self.g,self.A]) + b"\n")

            # Receive serverB
            received = sock.recv(1024)
            self.serverB=self.toread(received)

            # Send encrypt msg
            sock.sendall(self.towrite(self.encmsg("prout")))

        finally:
            sock.close()

        print ("Sent:     {}\n".format(self.A))
        print ("Received B from attaquant: {}\n".format(self.serverB))

if __name__ == "__main__":
    client=Myclient("localhost",4242)
    client.communication()
