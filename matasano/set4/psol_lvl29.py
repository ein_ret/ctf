#!/usr/bin/python2
# -*- coding: utf-8 -*-

import random,string,struct,os,hashlib,binascii
from Crypto.Util.strxor import strxor
from Crypto.Cipher import AES
from base64 import b64decode,b64encode
from struct import pack, unpack


def sha1(data,h0=0x67452301,h1 = 0xEFCDAB89,h2 = 0x98BADCFE,h3 = 0x10325476,h4 = 0xC3D2E1F0,lenght=None):
    """ Returns the SHA1 sum as a 40-character hex string """
    #h0 = 0x67452301
    #h1 = 0xEFCDAB89
    #h2 = 0x98BADCFE
    #h3 = 0x10325476
    #h4 = 0xC3D2E1F0

    if lenght is None :
        lenght=len(data)*8

    def rol(n, b):
        return ((n << b) | (n >> (32 - b))) & 0xffffffff

    # After the data, append a '1' bit, then pad data to a multiple of 64 bytes
    # (512 bits).  The last 64 bits must contain the length of the original
    # string in bits, so leave room for that (adding a whole padding block if
    # necessary).

    padding = chr(128) + chr(0) * (55 - len(data) % 64)
    if len(data) % 64 > 55:
        padding += chr(0) * (64 + 55 - len(data) % 64)
    padded_data = data + padding + pack('>Q', lenght)
    #print "padded_data sha1 function :\n%s" %(list(padded_data))

    thunks = [padded_data[i:i+64] for i in range(0, len(padded_data), 64)]
    print thunks
    for thunk in thunks:
        w = list(unpack('>16L', thunk)) + [0] * 64
        for i in range(16, 80):
            w[i] = rol((w[i-3] ^ w[i-8] ^ w[i-14] ^ w[i-16]), 1)

        a, b, c, d, e = h0, h1, h2, h3, h4
        print [hex(i) for i in a,b,c,d,e]
        # Main loop
        for i in range(0, 80):
            if 0 <= i < 20:
                f = (b & c) | ((~b) & d)
                k = 0x5A827999
            elif 20 <= i < 40:
                f = b ^ c ^ d
                k = 0x6ED9EBA1
            elif 40 <= i < 60:
                f = (b & c) | (b & d) | (c & d)
                k = 0x8F1BBCDC
            elif 60 <= i < 80:
                f = b ^ c ^ d
                k = 0xCA62C1D6

            a, b, c, d, e = rol(a, 5) + f + e + k + w[i] & 0xffffffff, \
                            a, rol(b, 30), c, d

        h0 = h0 + a & 0xffffffff
        h1 = h1 + b & 0xffffffff
        h2 = h2 + c & 0xffffffff
        h3 = h3 + d & 0xffffffff
        h4 = h4 + e & 0xffffffff

    return '%08x%08x%08x%08x%08x' % (h0, h1, h2, h3, h4)


secret=os.urandom(random.randint(20,60))

def validatemac(message,mac):
    print "==== validate mac ====\n\n"
    print "message %s\nmac_user %s\nmac_expected %s" %(list(message),mac,crappymac(message))
    rez=crappymac(message)==mac
    print "\n\n==== end ===="
    return rez

#crappymac is the function which generate the hmac with unknow secret
def crappymac(string):
    return sha1(secret+string)

#Le SHA-1 add at the end of msg one bit set to 1 following by multiple bits set to 0, then  the length of initial message (en bits) encoded on 64 bits The number of 0 have a length tin order to ensure that message is modulo 512 bits. The algo works on 512 bits bloc

#Function which perform the padding to fullfill the current bloc / need to bf if we don't know the length of the secret
def pad(string,lensecret):
   pad=string
   pad+="\x80"
   pad+="\x00"*(55-(len(string)+lensecret)%64) #64-55-1 (\x80) =8 so 8 bytes reauiered for the length of initial message
   pad+=struct.pack(">Q",8*(len(string)+lensecret)) #length in bits not byte....
   print "padperso :\n%s" %(list (pad))
   return pad

def forgedPayload(message,append,sizeblock,maxkeylen=200):
    mac=crappymac (message)
    print "mac(s+m) = %s\n\n" %(mac)


    #we generate the hash by using the previous state which is known and include the secret, we just need to adjust the length and consider the initial message
    h0,h1,h2,h3,h4=struct.unpack(">5I",binascii.unhexlify(mac))
    for i in xrange(maxkeylen):
        #Time to time it failed .... because the length is not correct, compute on len("append")*8 whereas the previous bloc of 512 bits should be count so 512+len("append"*8)
        # just restart the programme till it works .... no more time to invest :( 
        lentotalhash=len(suffix)*8 + ((len(pad(message,i)*8)/sizeblock)+1)*sizeblock
        #print "lentotalhash %i" %(lentotalhash)

        messagehack=pad(message,i)+suffix
        #print "machack : h(append,state=xxx) = %s\n\n" %(machack)

        machack=sha1(suffix,h0,h1,h2,h3,h4,lentotalhash)

        if validatemac(messagehack,machack):
            print "key is %d long" %(i)
            return messagehack,machack

    raise Exception("unexpected : maxkeylen reach... probably key is longer")


message="comment1=cooking%20MCs;userdata=foo;comment2=%20like%20a%20pound%20of%20bacon"
suffix=";admin=true"
sizeblock=512 #blocksha1 en bits

messagehack,machack=forgedPayload(message,suffix,512)

print "validation mac : %s" %(validatemac(messagehack,machack))

#print "mac(s+pad+append) = %s\n\n" %(crappymac(pad(message,16)+suffix))
