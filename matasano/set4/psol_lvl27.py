#!/usr/bin/python2
# -*- coding: utf-8 -*-

import random,string,struct,os,re
from Crypto.Util.strxor import strxor
from Crypto.Cipher import AES
from base64 import b64decode,b64encode


def delchar(s):
    s=re.sub('([=|;])','',s)
    return s

def padding(block,sizeblock=16):
    lenpad=-len(block)%sizeblock
    return block+struct.pack("B",lenpad)*lenpad

def aescbc_enc16(cleart):
    cleart="comment1=cooking%20MCs;userdata="+delchar(cleart)+";comment2=%20like%20a%20pound%20of%20bacon"
    aes=AES.new(keyfixed,AES.MODE_CBC,iv)
    return aes.encrypt(padding(cleart))


def aescbc_dec16(cipher):
    aes=AES.new(keyfixed,AES.MODE_CBC,iv)
    clear=aes.decrypt(cipher)
    if all([struct.unpack("B",val)[0] <128 for val in clear]):
        #print clear
        return re.search(";admin=true",clear)!= None
    raise ValueError (clear)

def decivkey(cipher):
    newciph=cipher[:16]+"\x00"*16+cipher[:16]

    try :
        aescbc_dec16(newciph)
    #we know that c1 use iv, for the cipher c1,0,c1 so we have the following plaintext
    #p'1=D(c1)^IV
    #p'3=D(c1)^0
    #donc p'1^p'3=IV=Key
    except ValueError as e:
        print "error server speak too much"
        clearjunk= e.args[0]
        return list(strxor(clearjunk[:16],clearjunk[32:]))


keyfixed=os.urandom(16)
print list (keyfixed)
iv=keyfixed

text="junkdata"
cipher=aescbc_enc16(text)
print "key retrieve "
print decivkey(cipher)
