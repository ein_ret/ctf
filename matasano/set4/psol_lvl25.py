#!/usr/bin/python2
# -*- coding: utf-8 -*-
import random,string,struct,os
from Crypto.Util.strxor import strxor
from Crypto.Cipher import AES
from base64 import b64decode,b64encode

def aesctr(text,key,nonce,b64=False):
    if b64 :
        text=b64decode(text)

    aesecb=AES.new(key,AES.MODE_ECB)
    rez=""
    counter=0
    stream=""

    #16*counter = len total stream -counter =16octets
    while (16*counter)< len(text) :
        stream=aesecb.encrypt(struct.pack("Qq",nonce,counter)) #Q=8 octets
        if len(text[16*counter:16*(counter+1)]) < 16 : #16=len stream
            stream=stream[:len(text[16*counter:16*(counter+1)])]
            rez+=strxor(stream,text[16*counter:16*(counter+1)])
            break

        rez+=strxor(stream,text[16*counter:16*(counter+1)])
        counter+=1
    return rez


def edit(ciphertext, offset, newtext):
    cleart=aesctr(ciphertext,key,nonce)
    if (offset+len(newtext)) <= len(cleart):
        cleart=cleart[:offset]+newtext+cleart[offset+len(newtext):]
        return aesctr(cleart,key,nonce)
    else :
        print ("offset+newtext too long")


def decrypt(ciphertext):
    #b=unknow^k et b'=junk^k donc b^b'=unknow^junk game over coz the function edit reuse the same nonce/key to decipher
    junk="x"*len(ciphertext)
    cjunk=edit(ciphertext,0,junk)
    tmp=strxor(cjunk,ciphertext)
    return strxor(tmp,junk)

key=os.urandom(16)
nonce=random.getrandbits(64)
text="duper super text!!!!"
ciph=aesctr(text,key,nonce)
print decrypt(ciph)
