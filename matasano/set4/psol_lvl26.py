#!/usr/bin/python2
# -*- coding: utf-8 -*-
import random,string,struct,os
from Crypto.Util.strxor import strxor
from Crypto.Cipher import AES
from base64 import b64decode,b64encode

def aesctr(text,key,nonce,b64=False):
    if b64 :
        text=b64decode(text)

    aesecb=AES.new(key,AES.MODE_ECB)
    rez=""
    counter=0
    stream=""

    #16*counter = len total stream -counter =16octets
    while (16*counter)< len(text) :
        stream=aesecb.encrypt(struct.pack("Qq",nonce,counter)) #Q=8 octets
        if len(text[16*counter:16*(counter+1)]) < 16 : #16=len stream
            stream=stream[:len(text[16*counter:16*(counter+1)])]
            rez+=strxor(stream,text[16*counter:16*(counter+1)])
            break

        rez+=strxor(stream,text[16*counter:16*(counter+1)])
        counter+=1
    return rez

def gettxt(text):
    text=re.sub('([=|;])','',text)
    prefix="comment1=cooking%20MCs;userdata="
    suffix=";comment2=%20like%20a%20pound%20of%20bacon"
    return prefix+text+suffix

key=os.urandom(16)
nonce=random.getrandbits(64)
text="adminXtrue"
ciph=list(aesctr(text,key,nonce))
print list(ciph)

print struct.unpack("B",ciph[5])[0]
ciph[5]=struct.pack("B",struct.unpack("B",ciph[5])[0]^61^88)
ciph="".join(ciph)
print list (aesctr(ciph,key,nonce))
