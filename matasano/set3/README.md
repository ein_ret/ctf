# Additional explanation

## level 17

### The intermediate state

The source is available there
[original source](https://robertheaton.com/2013/07/29/padding-oracle-attack/)

To repeat - in CBC encryption, each block of plaintext is XORed with the previous ciphertext block before being passed into the cipher. So in CBC decryption, each ciphertext is passed through the cipher, then XORed with the previous ciphertext block to give the plaintext.

![is0](png/is0.png)

The attack works by calculating the *intermediate state* of the decryption (see diagram) for each ciphertext. This is the state of a ciphertext block after being decrypted by the block cipher but before being XORed with the previous ciphertext block. We do this by working up from the plaintext rather than down through the block cipher, and don’t have to worry about the key or even the type of algorithm used in the block cipher.
Why is the intermediate state so important? Notice that:
```
I2 = C1 ^ P2
and
P2 = C1 ^ I2
```

We know C1 already, as it is just part of our ciphertext, so if we find I2 then we can trivially find P2 and decrypt the ciphertext.

![is1](png/is1.png)

Manipulating the ciphertext
Remember that we can pass in any ciphertext, and the server will tell us whether it decrypts to plaintext with valid padding or not. That’s it. We exploit this by passing in `C1' + C2`, where `C1'`is a sneakily chosen ciphertext block, `C2` is the ciphertext block we are trying to decrypt, and `C1' + C2` is the concatenation of the two. We call the decrypted plaintext block produced `P'2`.
To begin with, we choose `C1'[1..15]` to be random bytes, and `C1'[16]` to be `00`. We pass `C1' + C2` to the server. If the server says we have produced a plaintext with valid padding, then we can be pretty sure that `P2'[16]` must be `01` (as this would give us valid padding). Of course, if the server comes back and tells us that our padding is invalid, then we just set `C1'[16] to 01, then 02, and so on,` until we hit the jackpot.

![is2](png/is2.png)

Lets say that it turns out that `C1'[16] = 94` gives us valid padding. So now we have:

```
I2     = C1'     ^ P2'
I2[16] = C1'[16] ^ P2'[16]
       = 94      ^ 01
       = 95
```
We now know the final byte of the intermediate state! Notice that since `C2` is the same as it is in the real ciphertext, `I2` is also the same as in the real ciphertext. We can therefore go back to the ciphertext we are trying to decrypt:
```
P2[16] = C1[16] ^ I2[16]
       = C1[16] ^ 95
```
We plugin in whatever `C1[16]` is and find the last byte of the actual ciphertext! At this stage this will just be padding, so we will have to do some more decrypting before we find something interesting.
Do it again
We found the last byte by fiddling with `C1'` until we produced something with valid padding, and in doing so were able to infer that the final byte of `P'2 was 01`. We then used the fact that we knew `P2'[16]` and `C1'[16]` to find `I2[16]`. We continue on this theme to find the rest of the bytes of `I2`, and therefore decrypt the ciphertext block.
We now choose `C1'[1..14]` to be random bytes, `C1'[15]` to be the byte `00`, and `C1'[16] `to be a byte chosen so as to make

```
P2'[16] == 02:
C'1[16] = P'2[16] ^ I2[16]
        = 02      ^ 95
        = 93
```
So we can be sure that `P2'` will end in a `02`, and therefore the only way for `P2'` to have valid padding is if `P2[15]` is also `02!` We fiddle with` C1'[15]` until the server does its things and tells us we have passed it a ciphertext that decrypts to a plaintext with valid padding. Say this happens when `C1'[15] = 106 `- we do exactly what we did before:

```
I2     = C1'     ^ P2`
I2[15] = C1'[15] ^ P2'[15]
       = 106     ^ 02
       = 104
```

And presto, we know the second last byte of `I2` as well. We can therefore find the second last byte of `P2`, the real plaintext, again in exactly the same way as before:
```
P2[15] = C1[15] ^ I2[15]
       = C1[15] ^ 104
```   
Rinse, repeat, and read the entire 16 bytes of `C2!`
