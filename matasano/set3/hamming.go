package main

import (
"fmt"
"strconv"
"flag"
"os"
)

func main() {
    var param1 = flag.String("str1","","first string")
    var param2 = flag.String("str2","","second string")
    flag.Parse()

    if *param1 == "" || *param2 =="" || flag.NFlag() == 0 {
        fmt.Printf("Usage : ./hamming -str1 <string1> -str2 <string2>\n")
        os.Exit(1)
    }
    str1:=[]byte(*param1)
    str2:=[]byte(*param2)
  if len(str1) != len(str2) {
        fmt.Printf ("Length of the string is different, hamming distance cannot be computed\n ")
        os.Exit(1)
    }

    dst:=make([]byte,len(str1))
    hamming:=0
    
    for i,_:=range(str1) {
        dst[i]=str1[i]^str2[i]
        strbin:=strconv.FormatInt(int64(dst[i]),2)
        for _,v:= range (strbin) {
            i,_:=strconv.Atoi(string(v))
            hamming+=i
        }
    }
    fmt.Printf("hamming dst: %v\n",hamming)
}

