#!/usr/bin/python2
# -*- coding: utf-8 -*-
from base64 import *
from Crypto.Cipher import AES
import struct
from Crypto.Util.strxor import strxor,strxor_c
import binascii,os,random

keyfixed=os.urandom(16)
iv=os.urandom(16)

def padding(block,sizeblock=16):
    lenpad=-len(block)%sizeblock
    return block+struct.pack("B",lenpad)*lenpad


def cryptrnde():
    rndstr=[
    "MDAwMDAwTm93IHRoYXQgdGhlIHBhcnR5IGlzIGp1bXBpbmc=",
    "MDAwMDAxV2l0aCB0aGUgYmFzcyBraWNrZWQgaW4gYW5kIHRoZSBWZWdhJ3MgYXJlIHB1bXBpbic=",
    "MDAwMDAyUXVpY2sgdG8gdGhlIHBvaW50LCB0byB0aGUgcG9pbnQsIG5vIGZha2luZw==",
    "MDAwMDAzQ29va2luZyBNQydzIGxpa2UgYSBwb3VuZCBvZiBiYWNvbg==",
    "MDAwMDA0QnVybmluZyAnZW0sIGlmIHlvdSBhaW4ndCBxdWljayBhbmQgbmltYmxl",
    "MDAwMDA1SSBnbyBjcmF6eSB3aGVuIEkgaGVhciBhIGN5bWJhbA==",
    "MDAwMDA2QW5kIGEgaGlnaCBoYXQgd2l0aCBhIHNvdXBlZCB1cCB0ZW1wbw==",
    "MDAwMDA3SSdtIG9uIGEgcm9sbCwgaXQncyB0aW1lIHRvIGdvIHNvbG8=",
    "MDAwMDA4b2xsaW4nIGluIG15IGZpdmUgcG9pbnQgb2g=",
    "MDAwMDA5aXRoIG15IHJhZy10b3AgZG93biBzbyBteSBoYWlyIGNhbiBibG93"
    ]

    aes=AES.new(keyfixed,AES.MODE_CBC,iv)
    return aes.encrypt(padding(b64decode(rndstr[random.randint(0,len(rndstr)-1)])))

def decryptrnd(cipher):
    aes=AES.new(keyfixed,AES.MODE_CBC,iv)
    cleartx=aes.decrypt(cipher)
    try :
        checkpkcs7(cleartx)
        return True
    except ValueError :
        return False

def checkpkcs7(chaine):
    valpad=struct.unpack("B",chaine[-1])[0]
    if (chaine[-valpad:]==chaine[-1]*valpad) & ((-len(chaine[:-valpad])%16 == valpad) or (-len(chaine[:-valpad])%16 == 0)): #(-len(chaine[:-valpad])%16 == 0)) cas particulier ou padding de 16 ...
        print "Padding pkcs7 ok: %s" %(chaine[:-valpad])
        return chaine[:-valpad]
    else :
        raise ValueError("Bad padding pkcs#7")

def paddingOracleOneBlock(allcipher,numblock,blocksize=16):
    blockciph=[allcipher[i:i+16] for i in xrange (0,len(allcipher),16)]
    tmpb=["X"]*16
    IntermediateState=[]
    cleart=""
    while len(cleart)<blocksize:
        for i in xrange(256):
            bytetobf=blocksize-(len(IntermediateState)+1)
            tmpb[bytetobf]=struct.pack("B",i)
            concat="".join(tmpb)+blockciph[numblock]
            if decryptrnd(concat):
                index=1
                cleart+=chr(i^(len(IntermediateState)+1)^struct.unpack("B",blockciph[numblock-1][bytetobf])[0])
                IntermediateState.append(i^(len(IntermediateState)+1))
                print "intermediare %s" %(list(IntermediateState))
                #change the bytetobf and bytetobf+x
                #tmpb[bytetobf]=struct.pack("B",IntermediateState[len(cleart)-1]^(len(IntermediateState)+1))
                tmpb=tmpb[:-len(IntermediateState)]+[struct.pack("B",(len(IntermediateState)+1)^intp) for intp in IntermediateState[::-1]]
                print "tmpb :%s" %(list(tmpb))
                print cleart[::-1]
                break
    return cleart[::-1]

def paddingOracle(cipher):
    cleart=""
    for i in xrange (1,len(cipher)/16):
        cleart+=paddingOracleOneBlock(cipher,i)
    return cleart

cipher= cryptrnde()
print "\n\n=== PADDING ORACLE ===\nDunno IV, first block can't be decrypted\n\ndecrypt : %s" %(paddingOracle(cipher))
