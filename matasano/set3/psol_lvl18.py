#!/usr/bin/python2
# -*- coding: utf-8 -*-
from base64 import *
from Crypto.Cipher import AES
import struct
from Crypto.Util.strxor import strxor,strxor_c
import binascii,os,random

cipher="L77na/nrFsKvynd6HzOoG7GHTLXsTVu9qvY/2syLXzhPweyyMTJULu/6/kXX0KSvoOLSFQ=="
key="YELLOW SUBMARINE"
nonce=0

def aesctr(text,key,nonce):
    text=b64decode(text)
    aesecb=AES.new(key,AES.MODE_ECB)
    rez=""
    counter=0
    stream=""

    #16*counter = len total stream
    while (16*counter)< len(text) :
        stream=aesecb.encrypt(struct.pack("Qq",nonce,counter))
        if len(text[16*counter:16*(counter+1)]) < 16 : #16=len stream
            stream=stream[:len(text[16*counter:16*(counter+1)])]
            rez+=strxor(stream,text[16*counter:16*(counter+1)])
            break

        rez+=strxor(stream,text[16*counter:16*(counter+1)])
        counter+=1
    return rez

print aesctr(cipher,key,nonce)
