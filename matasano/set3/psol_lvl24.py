#!/usr/bin/python2
# -*- coding: utf-8 -*-


import random,string
from Crypto.Util.strxor import strxor

#wikipedia python code ....

def _int32(x):
    # Get the 32 least significant bits.
    return int(0xFFFFFFFF & x)

class MT19937:

    def __init__(self, seed):
        # Initialize the index to 0
        self.index = 624
        self.mt = [0] * 624
        self.mt[0] = seed  # Initialize the initial state to the seed
        for i in range(1, 624):
            self.mt[i] = _int32(
                1812433253 * (self.mt[i - 1] ^ self.mt[i - 1] >> 30) + i)

    def setmt (self,internalstate):
        self.mt=internalstate


    def extract_number(self):
        if self.index >= 624:
            self.twist()

        y = self.mt[self.index]
        #print "debug ext : %i"%(y)
        # Right shift by 11 bits
        y = y ^ y >> 11
        #print "y^y>>11 : %i"%(y)
        # Shift y left by 7 and take the bitwise and of 2636928640
        y = y ^ y << 7 & 2636928640
        #print "y^y<<7 &2636928640 : %i"%(y)
        # Shift y left by 15 and take the bitwise and of y and 4022730752
        y = y ^ y << 15 & 4022730752
        #print "y^y<<15&4022730752 : %i"%(y)
        # Right shift by 18 bits
        y = y ^ y >> 18
        #print "y^y>>18 : %i"%(y)
        self.index = self.index + 1

        return _int32(y)

    def twist(self):
        for i in range(0, 624):
            # Get the most significant bit and add it to the less significant
            # bits of the next number
            y = _int32((self.mt[i] & 0x80000000) +
                       (self.mt[(i + 1) % 624] & 0x7fffffff))
            self.mt[i] = self.mt[(i + 397) % 624] ^ y >> 1

            if y % 2 != 0:
                self.mt[i] = self.mt[i] ^ 0x9908b0df
        self.index = 0

seed=random.getrandbits(16)

def oracleenc(text):
    ran=random.randint(4,10)
    prefix="".join(random.sample(string.ascii_letters+string.digits,ran))
    text=prefix+text
    print "text :%s" %(text)
    mt=MT19937(seed)
    keystream=''
    while len(keystream) <  len(text):
        keystream+=str(mt.extract_number())
        #print len(keystream)
    keystream=keystream[:len(text)]
    return strxor(text,keystream)

def decypher():
    tmp="junk"*14
    ciph=oracleenc(tmp)
    prefixl=len(ciph)-len(tmp)
    print "size pre :%i " %(prefixl)
    tmp="x"*prefixl+tmp
    #seed is 16 length, we bf it
    for i in xrange ((1<<16)-1):
        #we encrypt // we can add the cipher method in the mt19937 class ....
        mt=MT19937(i)
        keystream=''
        while len(keystream) <  len(tmp):
            keystream+=str(mt.extract_number())
        keystream=keystream[:len(tmp)]
        ciphbf=strxor(tmp,keystream)
        #we caompare our cipher with the oracle till we get equal
        if (ciphbf[prefixl:]==ciph[prefixl:]):
            print "seed found :%i" %(i)
            break
    return i

def pwdresettoken(): #useless comme partie de challenge
    mt=MT19937(int(time.time()))
    rez=''
    keystream=''
    text=os.urandom(32)
    while len(keystream) <  len(text):
        keystream+=str(mt.extract_number())
    keystream=keystream[:len(text)]
    rez=strxor(text,keystream)
    return base64.b64encode(rez)


text="A"*14

ciph=oracleenc(text)
print "seed :%s" %(seed)
print "encrypt :%s" %(list(ciph))
decypher()
