#!/usr/bin/python2
# -*- coding: utf-8 -*-

import random
#wikipedia python code ....

def _int32(x):
    # Get the 32 least significant bits.
    return int(0xFFFFFFFF & x)

class MT19937:

    def __init__(self, seed):
        # Initialize the index to 0
        self.index = 624
        self.mt = [0] * 624
        self.mt[0] = seed  # Initialize the initial state to the seed
        for i in range(1, 624):
            self.mt[i] = _int32(
                1812433253 * (self.mt[i - 1] ^ self.mt[i - 1] >> 30) + i)

    def setmt (self,internalstate):
        self.mt=internalstate


    def extract_number(self):
        if self.index >= 624:
            self.twist()

        y = self.mt[self.index]
        #print "debug ext : %i"%(y)
        # Right shift by 11 bits
        y = y ^ y >> 11
        #print "y^y>>11 : %i"%(y)
        # Shift y left by 7 and take the bitwise and of 2636928640
        y = y ^ y << 7 & 2636928640
        #print "y^y<<7 &2636928640 : %i"%(y)
        # Shift y left by 15 and take the bitwise and of y and 4022730752
        y = y ^ y << 15 & 4022730752
        #print "y^y<<15&4022730752 : %i"%(y)
        # Right shift by 18 bits
        y = y ^ y >> 18
        #print "y^y>>18 : %i"%(y)
        self.index = self.index + 1

        return _int32(y)

    def twist(self):
        for i in range(0, 624):
            # Get the most significant bit and add it to the less significant
            # bits of the next number
            y = _int32((self.mt[i] & 0x80000000) +
                       (self.mt[(i + 1) % 624] & 0x7fffffff))
            self.mt[i] = self.mt[(i + 397) % 624] ^ y >> 1

            if y % 2 != 0:
                self.mt[i] = self.mt[i] ^ 0x9908b0df
        self.index = 0

def getmask(nbrV,offset):
    return (  ((1<<nbrV)-1)<<offset )

def r18 (v):
    #y ^ y >> 18 means 18 high bits are kept, we just need to shift by 18 18 and xored
    print v^(v>>18)
    return v^(v>>18)


def l15 (v) :
    #high 0b1111100000 low
    #y^ (y<<15 & 4022730752) //0xefc60000, xor is performed on the value masked, for the bits 0 du mask so 0^v=v. it is needed to take the complement mask to get the initial value
    maskComplement=0xefc60000 ^ 0xFFFFFFFF #on connait les 17 low bits = 0b10000001110011111111111111111
    y=v&maskComplement
    #since the shift is by 15, it is needed to reapply shift <<15 & mask on y to get the the 15th high bits miss (initial) and xored with v to get initial value
    mask15=getmask(15,32-15)
    y|= v^(y<<15 & 4022730752 ) & mask15 #on garde les 15 high bits
    print y
    #since the mask which give the 17th low bites and the shift of 15, it is needed to make a return v<<15 & 4022730752
    return y


def l7(v) :
    #y ^ (y<<7 & 2636928640 ) //0x9d2c5680
    maskComplement= 0x9d2c5680 ^ 0xFFFFFFFF #we know the 7 low bits 0b1100010110100111010100101111111
    y=v&maskComplement
    #we know the 7 low bits, shift by 7, we guess the 8-14th low bits
    # le & est prioritaire sur le ^ ........... obligatoire de ()
    y|= (v^ (y<<7 & 2636928640)) & getmask(7,7)
    #same for 15-21
    y|= (v^(y<<7 & 2636928640)) & getmask(7,14)
    y|=(v^(y<<7 & 2636928640)) & getmask(7,21)
    y|=(v^(y<<7 & 2636928640)) & getmask(7,28)
    y|=(v^(y<<7 & 2636928640)) & getmask(4,29)
    print y
    return y

def r11(v):
    #y ^ y >> 11
    #les 11 first bits are kept
    maskComplement =getmask(11,32-11)
    y=v&maskComplement
    #we know the 11 first bits so by xoring the first 11 with v, we get the intiial 21-11 high bits
    y|=(v^y>>11) & getmask(11,10)
    #idem for the last bits
    y|=(v^y>>11) & getmask(10,0)
    print y
    return y

def revmt19937(v):
    t1=r18(v)
    t2=l15(t1)
    t3=l7(t2)
    t4=r11(t3)
    return t4

seed=random.getrandbits(32)
mt=MT19937(seed)
internalState=list()

for i in xrange (624):
    internalState.append(revmt19937(mt.extract_number()))

print "\n\n === cloneMT19937 ==="
mtclone=MT19937(1337)
mtclone.setmt(internalState)

for i in xrange(20):
    print "mt19937 : %i cloneMt : %i check %s" %(mt.extract_number(),mtclone.extract_number(),mt.extract_number()==mtclone.extract_number())
