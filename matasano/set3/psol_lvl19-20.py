#!/usr/bin/python2
# -*- coding: utf-8 -*-
from base64 import *
from Crypto.Cipher import AES
import struct
from Crypto.Util.strxor import strxor,strxor_c
import binascii,os,random,itertools,string
from itertools import cycle,combinations,izip_longest


key=os.urandom(16)
nonce=0
text=["SSBoYXZlIG1ldCB0aGVtIGF0IGNsb3NlIG9mIGRheQ==",
"Q29taW5nIHdpdGggdml2aWQgZmFjZXM=",
"RnJvbSBjb3VudGVyIG9yIGRlc2sgYW1vbmcgZ3JleQ==",
"RWlnaHRlZW50aC1jZW50dXJ5IGhvdXNlcy4=",
"SSBoYXZlIHBhc3NlZCB3aXRoIGEgbm9kIG9mIHRoZSBoZWFk",
"T3IgcG9saXRlIG1lYW5pbmdsZXNzIHdvcmRzLA==",
"T3IgaGF2ZSBsaW5nZXJlZCBhd2hpbGUgYW5kIHNhaWQ=",
"UG9saXRlIG1lYW5pbmdsZXNzIHdvcmRzLA==",
"QW5kIHRob3VnaHQgYmVmb3JlIEkgaGFkIGRvbmU=",
"T2YgYSBtb2NraW5nIHRhbGUgb3IgYSBnaWJl",
"VG8gcGxlYXNlIGEgY29tcGFuaW9u",
"QXJvdW5kIHRoZSBmaXJlIGF0IHRoZSBjbHViLA==",
"QmVpbmcgY2VydGFpbiB0aGF0IHRoZXkgYW5kIEk=",
"QnV0IGxpdmVkIHdoZXJlIG1vdGxleSBpcyB3b3JuOg==",
"QWxsIGNoYW5nZWQsIGNoYW5nZWQgdXR0ZXJseTo=",
"QSB0ZXJyaWJsZSBiZWF1dHkgaXMgYm9ybi4=",
"VGhhdCB3b21hbidzIGRheXMgd2VyZSBzcGVudA==",
"SW4gaWdub3JhbnQgZ29vZCB3aWxsLA==",
"SGVyIG5pZ2h0cyBpbiBhcmd1bWVudA==",
"VW50aWwgaGVyIHZvaWNlIGdyZXcgc2hyaWxsLg==",
"V2hhdCB2b2ljZSBtb3JlIHN3ZWV0IHRoYW4gaGVycw==",
"V2hlbiB5b3VuZyBhbmQgYmVhdXRpZnVsLA==",
"U2hlIHJvZGUgdG8gaGFycmllcnM/",
"VGhpcyBtYW4gaGFkIGtlcHQgYSBzY2hvb2w=",
"QW5kIHJvZGUgb3VyIHdpbmdlZCBob3JzZS4=",
"VGhpcyBvdGhlciBoaXMgaGVscGVyIGFuZCBmcmllbmQ=",
"V2FzIGNvbWluZyBpbnRvIGhpcyBmb3JjZTs=",
"SGUgbWlnaHQgaGF2ZSB3b24gZmFtZSBpbiB0aGUgZW5kLA==",
"U28gc2Vuc2l0aXZlIGhpcyBuYXR1cmUgc2VlbWVkLA==",
"U28gZGFyaW5nIGFuZCBzd2VldCBoaXMgdGhvdWdodC4=",
"VGhpcyBvdGhlciBtYW4gSSBoYWQgZHJlYW1lZA==",
"QSBkcnVua2VuLCB2YWluLWdsb3Jpb3VzIGxvdXQu",
"SGUgaGFkIGRvbmUgbW9zdCBiaXR0ZXIgd3Jvbmc=",
"VG8gc29tZSB3aG8gYXJlIG5lYXIgbXkgaGVhcnQs",
"WWV0IEkgbnVtYmVyIGhpbSBpbiB0aGUgc29uZzs=",
"SGUsIHRvbywgaGFzIHJlc2lnbmVkIGhpcyBwYXJ0",
"SW4gdGhlIGNhc3VhbCBjb21lZHk7",
"SGUsIHRvbywgaGFzIGJlZW4gY2hhbmdlZCBpbiBoaXMgdHVybiw=",
"VHJhbnNmb3JtZWQgdXR0ZXJseTo=",
"QSB0ZXJyaWJsZSBiZWF1dHkgaXMgYm9ybi4="]


def EnFreqletter(s):
    # frequency taken from http://www.apprendre-en-ligne.net/crypto/stat/anglais.html j'ai ajouté ' ' arbitrairement
    besten=0
    englishLetterFreq = {'E':0.1256, 'T': 0.0915, 'A': 0.0808, 'O':0.0747, 'I':0.0724, 'N':0.0738, 'S': 0.0659, 'H':0.0527, 'R': 0.0642, 'D': 0.0399, 'L': 0.0404, 'C':0.0318, 'U': 0.0279, 'M': 0.0260, 'W': 0.0189, 'F': 0.0217, 'G': 0.0180, 'Y': 0.0165, 'P': 0.0191, 'B': 0.0167, 'V': 0.01, 'K': 0.0063, 'J': 0.0014, 'X': 0.0021, 'Q': 0.009, 'Z': 0.007,' ':0.05}
    for lettre in s:
        if string.upper(lettre) in englishLetterFreq.keys():
            besten+= englishLetterFreq[string.upper(lettre)]
    return besten


def aesctr(text,key,nonce):
    text=b64decode(text)
    aesecb=AES.new(key,AES.MODE_ECB)
    rez=""
    counter=0
    stream=""

    #16*counter = len total stream
    while (16*counter)< len(text) :
        stream=aesecb.encrypt(struct.pack("Qq",nonce,counter))
        if len(text[16*counter:16*(counter+1)]) < 16 : #16=len stream
            stream=stream[:len(text[16*counter:16*(counter+1)])]
            rez+=strxor(stream,text[16*counter:16*(counter+1)])
            break

        rez+=strxor(stream,text[16*counter:16*(counter+1)])
        counter+=1
    return rez

def maxletter(s):
    # frequency taken from http://www.apprendre-en-ligne.net/crypto/stat/anglais.html j'ai ajouté ' ' arbitrairement
    besten=0
    englishLetterFreq = {'E':0.1256, 'T': 0.0915, 'A': 0.0808, 'O':0.0747, 'I':0.0724, 'N':0.0738, 'S': 0.0659, 'H':0.0527, 'R': 0.0642, 'D': 0.0399, 'L': 0.0404, 'C':0.0318, 'U': 0.0279, 'M': 0.0260, 'W': 0.0189, 'F': 0.0217, 'G': 0.0180, 'Y': 0.0165, 'P': 0.0191, 'B': 0.0167, 'V': 0.01, 'K': 0.0063, 'J': 0.0014, 'X': 0.0021, 'Q': 0.009, 'Z': 0.007,' ':0.05}
    for lettre in s:
        if string.upper(lettre) in englishLetterFreq.keys():
            besten+= englishLetterFreq[string.upper(lettre)]
    return besten


def singlexordech(chaine):
    rez=list()
    for i in xrange(256):
        rezt=[strxor_c(ele,i) for ele in chaine]
        rez.append([maxletter("".join(rezt)),i,"".join(rezt)])
    return sorted(rez,key=lambda x:x[0],reverse=True)[0]

def getkey(trans):
    rtmp=["".join(x) for x in trans]
    key=""
    for element in rtmp :
        indic,cleN,cleart=singlexordech(element)
        key+=chr(cleN)
    return key

cipherL=[aesctr(s,key,nonce) for s in text]
cipherL=sorted(cipherL,key=lambda x:len(x),reverse=True)

#print cipherL


#p1=c1^k et p2=c2^k then c1^c2=p1^p2
#space xor [a-z][A-Z] = [a-z][A-Z]
#if xor c1 with others and we get letters it means that character is a ' '
#easier we bf the 1st char on all ciphers and we take the high proba via maxletter, and so on ... we adjust after the first ten
dic=dict()

#zip function to rebuild each first value of lists
transpo=list(izip_longest(*cipherL,fillvalue='\x00'))
#brute force on each char
key=getkey(transpo)

for ele in cipherL :
    if len(ele)<len(key):
        print strxor(key[:len(ele)],ele)
    else :
        print strxor(key,ele[:len(key)])
