#challenge RSA forging signature

## 0x1 Context

The goal is to exploit the lack of padding validation combining that the public exponent is 3.

Usally in pkcs1.5 padding, the message should respect the following pattern


```
00 01        FF......FF     00        ASN1          HASH
fix byte       padding   delimiter   algo used
```

The vulnerability occurs if the padding `FF....FF` is not verified that means there is no control that the FF is really used to perform the padding. Moreover the public exponent is set to 3 so it could be possible to find a number which satisfy that

`m**3 = 00 01 XXXXXX ASN1  HASH with XXXXX any characters AND < N.`

Indeed to validate that the signature "sig" for the message "m" is correct, the following should be done :

```
step 1 : (sig ** 3) mod N  == 00 01        FF......FF     00        ASN1          HASH
step 2 : md5("challenge") == HASH
step 3 : sig is right

```
In our case it should be possible to find a sig ** 3 which is inferior to N that means the modulo N will never be compute. To sum up lets find a number
`k where k**3 < N and k**3 = 00 01 .............. 00 ASN1 HASH`


## 0x2 solution

lets start by the begining, in the source code of the page http://pkcs-is-bad.ctfcompetition.com/ the public key is given

```
<div id="adminPubKey" style="visibility:hidden">
-----BEGIN RSA PUBLIC KEY-----
MIGdMA0GCSqGSIb3DQEBAQUAA4GLADCBhwKBgQDXnmZBhgORgRu6gXwGplTHHIfV
Z1kXgC/o3cSDl8JbK14wMn3o3CPIlhbDFuyzapB3rkKcECP3uGKco4AoBf/CQDoH
ZJii5gL9YwXnUPul2wWCvTy2NyW0fkBpZwK85HtR4D6AwhHaCP6hhMPdj41spp4O
q6xbZ1E1zypmjToxVQIBAw==
-----END RSA PUBLIC KEY-----
</div>
```

which allow us to get the (N,e)

```
$ openssl rsa -pubin -in key.pub -text -noout
Public-Key: (1024 bit)
Modulus:
    00:d7:9e:66:41:86:03:91:81:1b:ba:81:7c:06:a6:
    54:c7:1c:87:d5:67:59:17:80:2f:e8:dd:c4:83:97:
    c2:5b:2b:5e:30:32:7d:e8:dc:23:c8:96:16:c3:16:
    ec:b3:6a:90:77:ae:42:9c:10:23:f7:b8:62:9c:a3:
    80:28:05:ff:c2:40:3a:07:64:98:a2:e6:02:fd:63:
    05:e7:50:fb:a5:db:05:82:bd:3c:b6:37:25:b4:7e:
    40:69:67:02:bc:e4:7b:51:e0:3e:80:c2:11:da:08:
    fe:a1:84:c3:dd:8f:8d:6c:a6:9e:0e:ab:ac:5b:67:
    51:35:cf:2a:66:8d:3a:31:55
Exponent: 3 (0x3)
```
For the conversion string to int, the libnum has been used, the core of the challenge is to find a way to find a number where the cube will be 00 ASN1 HASH

Lets take an example we have to forge "I" which is 73 = 1001001, in he following schema x represents a fail bit

```
sig       0000001   
cube      0000001
expected  1001001
          x  x

sig            0001001
cube        1011011001
expected       1001001
				 x

sig             0011001
cube     11110100001001
expected        1001001
                x
sig                   1011001
cube     10101100000111001001
expected              1001001

```

The hard part is done, till now we success to forge the lower part of the sig which when cubing we get the expected 00 ASN1 HASH
Regarding the upper part we just need to find a number where when it is cube start with 00 01 and ensure that it does not contain any null byte (remember the 00 is a delimiter for the ASN1)

Just merge the both sig and get the flags. Indeed, even if the upper sig is cut it doesnot change the 00 01 upper bits since we work with big number :

```
In [107]: bin(99999999999999999999999999999999999999999999999999999 **3)
Out[107]: '0b1001000110101011101101000010001011001100101110000001001011101110101011000110001011100000010101011100000100001010101100110011101010000010110011100011111110011110000011100001001011011011010101100011001101001000101001111001101111010110111011011111001001010011010111001110001011111010110100011001000001000000011110110011100000100011110001000001111011111010111110101000111110010011001011000111110111110101001010110001001100000000111101001000111010000010011111101001001011010101111011111111111111111111111111111111111111111111111111111'


In [108]: bin(9999999999999999999999999111111111111111111111111111 **3)
Out[108]: '0b100101010010101010110100010111001111101010010111101000001011001011011101100101000010101001011101110110001001010000011110111000100011110010000101000000000001010111110101010100011011001000010010011001101010011000110101110111000111100011101011011011101110010100111100011111001010001000010010011001000111100001000111110000110110000011010100010111110011111101110000110000000010011001100000100010000011101010001001101010010111111011101000001010100000101110111000010101110110010011100010001000101001111000110011010001010010111'
```

we constat that we keep the upper byte 10010

## 0x3 script execution

```
$ python rsa3root2.py
suffix which should be generated
003020300c06082a864886f70d020505000410b04ec0ade3d49b4a079f0e207d5e2821
check if s2n(suffix) &0x1  == 1 : True

partial sig suffix 1811001699402552326786184033966637715804247640188078160785587072877366600383016839521
len prefix 128 len suffix 35

check that suffix is in sig**3 True

check that sig**3 < n True

len sig 44 sig :
01 d8 e0 ea 1b 11 cb 40 0d c8 c9 10 79 c7 6a af e0 4b 19 02 2c 13 db a1 eb 60 5f ce df 62 52 29 a6 f2 6d ac 3b 6e b6 27 c8 c4 8b 3c 26 21 c3 b6 16 71 30 97 ca 4d eb 98 ad f5 65 a7 df 75 14 38 46 a8 fb e1 63 9e 45 55 a5 1b e6 ca 73 dc 94 1f 7f 15 99 d7 01 d3 d1 2f 79 f3 7f c9 00 30 20 30 0c 06 08 2a 86 48 86 f7 0d 02 05 05 00 04 10 b0 4e c0 ad e3 d4 9b 4a 07 9f 0e 20 7d 5e 28 21

base 64:
AAE6GzCKvu6P7qYz7fp1DSPIl3WUgEqZjC3h6AIuM3Wd1xMn41ZNDcXRqWE=
```

![code exec](png/code1.png "code exec")

![code exec](png/code2.png "code exec")

![flag](png/flag.png "flag")

## 0x4 reference
https://www.ietf.org/mail-archive/web/openpgp/current/msg00999.html
https://nvd.nist.gov/vuln/detail/CVE-2016-1494
https://blog.filippo.io/bleichenbacher-06-signature-forgery-in-python-rsa/
