# -*- coding: utf-8 --

import hashlib
from binascii import *
from libnum import *
import rsa,os
from base64 import *


def get_Rbit(n,i):
    #return the i eme bit
    return (((1 << i) &n) >> i)


n=151412633855954843819733434464125339052568898102931536911637903559841544558723973043050816338485284590514808104500875340749604123632852946469150713976173372290733260940828083371874628381129699367481235628443486831633794198594140144657045371665601505687547633943899965976948544127536969540495613484548832768341

e=3

#format to sign message for 1024 key (len = 128) 
#00 01   FF···FF  00  3020300c06082a864886f70d020505000410   hash
#         padding        asn1 magic md5 value


message="challenge"
h=hashlib.md5(message).digest()
md5_asn1=rsa.pkcs1.HASH_ASN1['MD5']

suffix="\x00"+md5_asn1+h
suffixnum=s2n(suffix)
print ("suffix which should be generated \n%s"%(hexlify(suffix)))
print ("check if s2n(suffix) &0x1  == 1 : %s"%(s2n(suffix)&0x1 == 0x1))

sig_suffix=1

for x in xrange (len(suffix)*8):
    if (get_Rbit(suffixnum,x) != get_Rbit(sig_suffix**3,x)):
        sig_suffix=(1<< x) | sig_suffix

print ("\npartial sig suffix %i"%(sig_suffix))

prefix=""
sig_prefix=0

while True:
    prefix="\x00\x01"+os.urandom(128-2)
    sig_prefix=nroot(s2n(prefix),3)
    ##print ("\x00" not in n2s(sig_prefix**3)[:-len(suffix)])
    if ("\x00" not in (n2s(sig_prefix**3)[:-len(suffix)])) :
        break

print ("len prefix %i len suffix %i"%(len(prefix),len(suffix)))
sig="\x00"+ n2s(sig_prefix)[:-len(n2s(sig_suffix))]+n2s(sig_suffix)


sigcheck=hexlify(n2s(s2n(sig)**3))

print ("\ncheck that suffix is in sig**3 %s"%(n2s(s2n(sig)**3).endswith(suffix)))
print ("\ncheck that sig**3 < n %s"%((s2n(sig)**3) < n ))

print ("\nlen sig %i sig :\n%s\n\nbase 64:\n%s" %(len(sig),' '.join([sigcheck[i:i+2] for i in xrange (0,len(sigcheck),2)]),b64encode(sig)))


