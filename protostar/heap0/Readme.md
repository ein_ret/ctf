# context

Trivial to see what to do, replace the pointer of fp to winner.
ASLR is disabled on the exercice plateform

# solution

Lets execute the binary
![1exe](png/1strun.PNG)

we know the addresse where are located the chunks, lets set a bp before strcpy and analyse a clean chunks

![memory](png/heapdb.PNG)

- At 0x0804a000 the begining of data chunk and the header (8 bytes)
- The length data struct is char name [64] soit 0x40 bytes, then the next struct of fp should start at 0x0804a008 + 0x40 = 0x0804a048, which is confirmed by the screenshot data of fp starts at 0x0804a050 which is 0x0804a050 -0x8 = 0x0804a048 (with header of fp chunks)

So lets grab the winner address
![addfunc](png/addressFunction.PNG)

Since the chunks are consecutive in memory, the exploit consist to overwrite the addresse 0x0804a050 with 0w08048464

![win](png/win.PNG)
