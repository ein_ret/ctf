import r2pipe,sys

r2 = r2pipe.open('rabbithole',flags=['-2'])
r2.cmd('e dbg.profile=profile.rr2')

flag = ''
r2.cmd('aaa')

#open in debugger
r2.cmd('doo')

#set bp to call check
main_addr=r2.cmd('f~main').split(' ')[0]
check_addr=int(main_addr,16)+147
print ("addr of check_value {}".format(hex(check_addr)))
r2.cmd('db {}'.format(hex(check_addr)))

for _ in xrange(60) :
    for ch in 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_+!{}':
        # Run till reach the bp
        r2.cmd('dc')

        #save rsi in case of fail
        old_rsi=r2.cmdj('drj')['rsi']

        #useless but set the stack
        r2.cmd('wz %s @ rsp+8'%format(flag+ch))
        #print(r2.cmd('px @rsp'))

        #overwrite the rdi with the character tested
        r2.cmd('dr rdi={}'.format(ord(ch)))

        #step over 2 instruction just afetr inc rdi
        r2.cmd('dso 2')

        #check register
        rax=r2.cmdj('drj')['rax']
        rdx=r2.cmdj('drj')['rdx']
        rdi=r2.cmdj('drj')['rdi']

        #if rax=0 it means that it is the wrong char, lets decrement rdx and retry
        if rax==0:
            rdx-=1
            r2.cmd('dr rdx={}'.format(rdx))
            r2.cmd('dr rip={}'.format(check_addr))
            r2.cmd('dr rsi={}'.format(old_rsi))
        else:
            flag+=ch
            print(flag)
            break
