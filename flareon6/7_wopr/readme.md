# Context

## Introduction

This is the walkthougth to 7-wopr of Flareon6 challenge

# Tools

* Ghidra
* pyinstxtractor.py _(it required to do a pip3.7 install --upgrade uncompyle6 to make it works ...)_
* uncompyl6
* x64dbg

# Static analysis

It appears by looking strings that the binary looks like a python script compiled

![py](png/py.PNG)

First attemp to decompile was done with python3.6 but a warnig message occurs and advice to use python3.7
```
python ~/tools/python-exe-unpacker/pyinstxtractor.py wopr.exe
[*] Processing wopr.exe
[*] Pyinstaller version: 2.1+
[*] Python version: 37
[*] Length of package: 5068358 bytes
[*] Found 64 files in CArchive
[*] Beginning extraction...please standby
[*] Found 135 files in PYZ archive
[*] Successfully extracted pyinstaller archive: wopr.exe
You can now use a python decompiler on the pyc files within the extracted directory
```

after renaming pyiboot01_bootstrap into pyiboot01_bootstrap.pyc, uncompyle6 returns an error when attempting to decompile pyc to py 

```
uncompyle6 pyiboot01_bootstrap.pyc 
Traceback (most recent call last):
  File "~/.virtualenvs/p37/lib/python3.7/site-packages/xdis/load.py", line 167, in load_module_from_file_object
    float_version = magic_int2float(magic_int)
  File "~/.virtualenvs/p37/lib/python3.7/site-packages/xdis/magics.py", line 368, in magic_int2float
    return py_str2float(magicint2version[magic_int])
KeyError: 227

During handling of the above exception, another exception occurred:

```

After digging a bit, I create a dummy get_header.py with `import os` compile it `python -m compileall get_header.py` and use an hexeditor to add the header to pyiboot01_bootstrap and pyiboot01_cleanup

![header](png/header.PNG)

We finally get a readable python code 

```
# uncompyle6 version 3.6.6
# Python bytecode 3.7 (3394)
# Decompiled from: Python 3.7.5 (default, Nov  7 2019, 10:50:52) 
# [GCC 8.3.0]
# Embedded file name: site-packages\PyInstaller\loader\pyiboot01_bootstrap.py
# Compiled at: 2020-04-23 13:19:53
# Size of source mod 2**32: 10 bytes
import sys, pyimod03_importers
pyimod03_importers.install()
import os
if not hasattr(sys, 'frozen'):
    sys.frozen = True
sys.prefix = sys._MEIPASS
sys.exec_prefix = sys.prefix
sys.base_prefix = sys.prefix
sys.base_exec_prefix = sys.exec_prefix
VIRTENV = 'VIRTUAL_ENV'
if VIRTENV in os.environ:
    os.environ[VIRTENV] = ''
    del os.environ[VIRTENV]
```

the python script can be found in the file `first_stage.py`

# Code analysis

## De-obfuscation 

Lets focus on `pyiboot02_cleanup.py` when executing as is the file we get an error

```
Traceback (most recent call last):
  File "pyiboot02_cleanup.py", line 12, in <module>
    BOUNCE = pkgutil.get_data('this', 'key')
  File "/usr/lib/python3.7/pkgutil.py", line 637, in get_data
    return loader.get_data(resource_name)
  File "<frozen importlib._bootstrap_external>", line 916, in get_data
FileNotFoundError: [Errno 2] No such file or directory: '/usr/lib/python3.7/key'
```

Lets replace the `BOUNCE= pkgutil.get_data('this', 'key')` directly with the value extracted from the file via `xxd -ps wopr.exe_extracted/this\\key`

```
BOUNCE = bytearray.fromhex("afbc6255fb09d....")
```

When we run again the script, I exepected code to be print but an error occurs :
```

print("DEBUG0")
for i in range(256):
    try:
        print(lzma.decompress(fire(eye(__doc__.encode()), bytes([i]) + BOUNCE)))
    except Exception as e:
        print ("Exception triggered "+ str(e))
        pass

## 

Traceback (most recent call last):
  File "pyiboot02_cleanup.py", line 79, in <module>
    print("DEBUG 0")
  File "<string>", line 1
    DEBUG 0
          ^
SyntaxError: invalid syntax

```        

By debugging the begining of the script, we understand that print and exec are switched, to confirm that we also change `print("DEBUG 0")` in `exec("DEBUG0")` :

```
a = 1702389091
b = 482955849332
g = ho(29516388843672123817340395359, globals())
aa = getattr(g, ho(a))
bb = getattr(g, ho(b))
print("DEBUG1 aa"+str(aa))
print("DEBUG2 aa"+str(bb))
a ^= b
b ^= a
a ^= b
print("DEBUG3 a "+str(a))
print("DEBUG4 b "+str(b))
setattr(g, ho(a), aa)
setattr(g, ho(b), bb)

### 
LOADING...
DEBUG1 aa<built-in function exec>
DEBUG2 aa<built-in function print>
DEBUG3 a 482955849332
DEBUG4 b 1702389091
DEBUG 0


```

However, we still don't get the result of `exec(lzma.decompress(fire(eye(__doc__.encode()), bytes([i]) + BOUNCE)))` that should print the result, by displaying the exception we get :

```
Traceback (most recent call last):
  File "pyiboot02_cleanup.py", line 81, in <module>
    exec(lzma.decompress(fire(eye(__doc__.encode()), bytes([i]) + BOUNCE)))
  File "/usr/lib/python3.7/lzma.py", line 334, in decompress
    res = decomp.decompress(data)
_lzma.LZMAError: Input format not supported by decoder

```

2 options here :

* BOUNCE is invalid
* `__doc__`  is invalid

`__doc__` is composed by text and various space and tablespace which are used in the eye function :

```
def eye(face):
    leg = io.BytesIO()
    for arm in face.splitlines():
        arm = arm[len(arm.rstrip(b' \t')):]
        leg.write(arm)
```

Lets get __doc__ directly form the original file since it is a string located at the begining of the file via an hexeditor, `__doc__` and `BOUNCE` are modified like this :

```
doc_hex = "0a4f6e63652075706f6e2061206d69646e6967687...skip content...2009202020092009090920092009200920092020202009090920"
doc_byte = bytes.fromhex(doc_hex)
__doc__= doc_byte.decode()
BOUNCE = bytes.fromhex("afbc6255fb09d05566174a455112cc144909c8ffe992489e9c7300a88453b4459d3b21b96510400cca2c3a58299e08f37486bbbf8e8dd8e1d8ce03785ba4d3afff8b2cc90b4876bf1a8a68045d3a3bd9d742b0acb87d60de0f16c4e101b4bbe5706d78c6941c83d9fe642773e26284948f4383ebcf87b2017e9baf00ac9849f231c872188c40e2c6350adc3696fbb8727583f4f3762ebb600e5dcba87728188e2fae50b5e4e14a0636a9f8466cbed40b36b2de6fb9f82934dbb0563db6817902cdb3e1aef78582848c959dde438f8c03adc85899e886f463a7271d86fe0ffb1f1226cd5391bd1188b263cb8da967eeff72b2efbc96afaaf7be8ff0681e86bd53")
```

## Analysis of the second stage

The code can be found in the file `second_stage.py`

By analysing it we saw that the user input is transform in an array b[16] and compare to `h`
```
target = input()

t.typewriteln("\nPREPARING NUCLEAR STRIKE FOR " + target.upper())
t.typewrite("ENTER LAUNCH CODE: ")
launch_code = input().encode()

# encoding map coordinates
x = list(launch_code.ljust(16, b''))
b = 16 * [None]

# calculate missile trajectory
b[0] = x[2] ^ x[3] ^ x[4] ^ x[8] ^ x[11] ^ x[14]
b[1] = x[0] ^ x[1] ^ x[8] ^ x[11] ^ x[13] ^ x[14]
b[2] = x[0] ^ x[1] ^ x[2] ^ x[4] ^ x[5] ^ x[8] ^ x[9] ^ x[10] ^ x[13] ^ x[14] ^ x[15]
b[3] = x[5] ^ x[6] ^ x[8] ^ x[9] ^ x[10] ^ x[12] ^ x[15]
b[4] = x[1] ^ x[6] ^ x[7] ^ x[8] ^ x[12] ^ x[13] ^ x[14] ^ x[15]
b[5] = x[0] ^ x[4] ^ x[7] ^ x[8] ^ x[9] ^ x[10] ^ x[12] ^ x[13] ^ x[14] ^ x[15]
b[6] = x[1] ^ x[3] ^ x[7] ^ x[9] ^ x[10] ^ x[11] ^ x[12] ^ x[13] ^ x[15]
b[7] = x[0] ^ x[1] ^ x[2] ^ x[3] ^ x[4] ^ x[8] ^ x[10] ^ x[11] ^ x[14]
b[8] = x[1] ^ x[2] ^ x[3] ^ x[5] ^ x[9] ^ x[10] ^ x[11] ^ x[12]
b[9] = x[6] ^ x[7] ^ x[8] ^ x[10] ^ x[11] ^ x[12] ^ x[15]
b[10] = x[0] ^ x[3] ^ x[4] ^ x[7] ^ x[8] ^ x[10] ^ x[11] ^ x[12] ^ x[13] ^ x[14] ^ x[15]
b[11] = x[0] ^ x[2] ^ x[4] ^ x[6] ^ x[13]
b[12] = x[0] ^ x[3] ^ x[6] ^ x[7] ^ x[10] ^ x[12] ^ x[15]
b[13] = x[2] ^ x[3] ^ x[4] ^ x[5] ^ x[6] ^ x[7] ^ x[11] ^ x[12] ^ x[13] ^ x[14]
b[14] = x[1] ^ x[2] ^ x[3] ^ x[5] ^ x[7] ^ x[11] ^ x[13] ^ x[14] ^ x[15]
b[15] = x[1] ^ x[3] ^ x[5] ^ x[9] ^ x[10] ^ x[11] ^ x[13] ^ x[15]

if b == h:
    t.typewriteln("LAUNCH CODE ACCEPTED.\n*** RUNNING SIMULATION ***\n")
```

h is previously computed by the wrong function :
```
xor = [212, 162, 242, 218, 101, 109, 50, 31, 125, 112, 249, 83, 55, 187, 131, 206]
h = list(wrong())
h = [h[i] ^ xor[i] for i in range(16)]

def wrong():
    trust = windll.kernel32.GetModuleHandleW(None)

    computer = string_at(trust, 1024)
    dirty, = struct.unpack_from('=I', computer, 60)
    [...]
```

so what we need to do is :

* Get the output value of wrong
* resolve the equation with 16 unknown to get b 



### Wrong()

The function starts with these instructions

```
trust = windll.kernel32.GetModuleHandleW(None) 
    computer = string_at(trust, 1024) 

``` 

* return the base address of the binary
* dump the 1000 first bytes

#### Dump

The first attempt was to use the task manager to dump the memory of the process and read the first 1000 bytes. 

![dump](png/dump.PNG)

However we get an error :

```
Traceback (most recent call last):
  File "wrong.py", line 51, in <module>
    wrong()    
  File "wrong.py", line 14, in wrong
    _, _, organize, _, _, _, variety, _ =  struct.unpack_from('=IHHIIIHH', computer, dirty)
struct.error: unpack_from requires a buffer of at least 24 bytes
(p37) @pbox:~/projects/re/flareon6/7_wopr$ 
```

If we looks the memory dump with an hexeditor, the output is formated with DMG magic word and looks like a specific format.

![wrong_dump](png/wrong_dump.PNG)

The same result with procdump from [sysinternal tools](https://docs.microsoft.com/en-us/sysinternals/downloads/)

Finally, the dump was successful with x64dbg

![how_to_dump](png/how_to_dump.PNG)

the hexdump show the 'MZ' magic world that looks far better

![good_dump](png/good_dump.PNG)

#### Modification of the wrong

The wrong function has been modified as follow :

* Read the dump 
* Change the `string_at` function with trust[a:a+b]
* The value if `trustaddr=0x860000` is given by x64 when we perform the memory dump

The final script can be found in the file `getH.py`

after execution of the script we finally get the value of `h`

```
python GetH.py 
[115, 29, 32, 68, 106, 108, 89, 76, 21, 71, 78, 51, 75, 1, 55, 102]
```

### Equation

The last part of the challenge is to resolve the following equation _(with b equals h)_:
```
b[0] = x[2] ^ x[3] ^ x[4] ^ x[8] ^ x[11] ^ x[14]
b[1] = x[0] ^ x[1] ^ x[8] ^ x[11] ^ x[13] ^ x[14]
b[2] = x[0] ^ x[1] ^ x[2] ^ x[4] ^ x[5] ^ x[8] ^ x[9] ^ x[10] ^ x[13] ^ x[14] ^ x[15]
b[3] = x[5] ^ x[6] ^ x[8] ^ x[9] ^ x[10] ^ x[12] ^ x[15]
b[4] = x[1] ^ x[6] ^ x[7] ^ x[8] ^ x[12] ^ x[13] ^ x[14] ^ x[15]
b[5] = x[0] ^ x[4] ^ x[7] ^ x[8] ^ x[9] ^ x[10] ^ x[12] ^ x[13] ^ x[14] ^ x[15]
b[6] = x[1] ^ x[3] ^ x[7] ^ x[9] ^ x[10] ^ x[11] ^ x[12] ^ x[13] ^ x[15]
b[7] = x[0] ^ x[1] ^ x[2] ^ x[3] ^ x[4] ^ x[8] ^ x[10] ^ x[11] ^ x[14]
b[8] = x[1] ^ x[2] ^ x[3] ^ x[5] ^ x[9] ^ x[10] ^ x[11] ^ x[12]
b[9] = x[6] ^ x[7] ^ x[8] ^ x[10] ^ x[11] ^ x[12] ^ x[15]
b[10] = x[0] ^ x[3] ^ x[4] ^ x[7] ^ x[8] ^ x[10] ^ x[11] ^ x[12] ^ x[13] ^ x[14] ^ x[15]
b[11] = x[0] ^ x[2] ^ x[4] ^ x[6] ^ x[13]
b[12] = x[0] ^ x[3] ^ x[6] ^ x[7] ^ x[10] ^ x[12] ^ x[15]
b[13] = x[2] ^ x[3] ^ x[4] ^ x[5] ^ x[6] ^ x[7] ^ x[11] ^ x[12] ^ x[13] ^ x[14]
b[14] = x[1] ^ x[2] ^ x[3] ^ x[5] ^ x[7] ^ x[11] ^ x[13] ^ x[14] ^ x[15]
b[15] = x[1] ^ x[3] ^ x[5] ^ x[9] ^ x[10] ^ x[11] ^ x[13] ^ x[15]
```

### Z3, binding go 

I decide to go with [z3](https://github.com/Z3Prover/z3) by using a Golang binding [go-z3](https://github.com/aclements/go-z3). However, the binding has not been updated since 3 years and multiples compilation errors occurs. To be able to use it :

1. Fork the [go-z3](https://github.com/aclements/go-z3) commit 18129d7.
2. Apply the different fix available in the branches [here](https://github.com/ajalab/go-z3/tree/fix-compile-failure).
3. Fix another issue regarding the z3 boolean.

The patch files are available in the directory `patch_z3_go_binding` included the custom fix.

The go code `main.go` provides us the code to enter :

![z3go](png/z3go.PNG)

`5C0G7TY2LWI2YXMC`