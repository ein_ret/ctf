package main

import (
	"fmt"
	"os"

	"github.com/aclements/go-z3/z3"
)

var (
	h = []int{115, 29, 32, 68, 106, 108, 89, 76, 21, 71, 78, 51, 75, 1, 55, 102}
)

func main() {
	fmt.Println("Preparing SAT...")
	ctx := z3.NewContext(nil)
	s := z3.NewSolver(ctx)

	var htmp z3.BV
	hmap := make(map[string]z3.BV)

	// Declare constants.
	for i, v := range h {
		htmp = ctx.FromInt(int64(v), ctx.BVSort(8)).(z3.BV)
		hmap[fmt.Sprintf("h%d", i)] = htmp
	}

	// Declare variables x0...x15
	xmap := make(map[string]z3.BV)
	var xi string
	for i := 0; i < 16; i++ {
		xi = fmt.Sprintf("x%d", i)
		xmap[xi] = ctx.BVConst(xi, 8)
	}
	// b[0] = x[2] ^ x[3] ^ x[4] ^ x[8] ^ x[11] ^ x[14]
	s.Assert(hmap["h0"].Eq((xmap["x2"].Xor(xmap["x3"]).Xor(xmap["x4"]).Xor(xmap["x8"]).Xor(xmap["x11"]).Xor(xmap["x14"]))))
	// b[1] = x[0] ^ x[1] ^ x[8] ^ x[11] ^ x[13] ^ x[14]
	s.Assert(hmap["h1"].Eq((xmap["x0"]).Xor(xmap["x1"]).Xor(xmap["x8"]).Xor(xmap["x11"]).Xor(xmap["x13"]).Xor(xmap["x14"])))
	// b[2] = x[0] ^ x[1] ^ x[2] ^ x[4] ^ x[5] ^ x[8] ^ x[9] ^ x[10] ^ x[13] ^ x[14] ^ x[15]
	s.Assert(hmap["h2"].Eq((xmap["x0"]).Xor(xmap["x1"]).Xor(xmap["x2"]).Xor(xmap["x4"]).Xor(xmap["x5"]).Xor(xmap["x8"]).Xor(xmap["x9"]).Xor(xmap["x10"]).Xor(xmap["x13"]).Xor(xmap["x14"]).Xor(xmap["x15"])))
	// b[3] = x[5] ^ x[6] ^ x[8] ^ x[9] ^ x[10] ^ x[12] ^ x[15]
	s.Assert(hmap["h3"].Eq((xmap["x5"]).Xor(xmap["x6"]).Xor(xmap["x8"]).Xor(xmap["x9"]).Xor(xmap["x10"]).Xor(xmap["x12"]).Xor(xmap["x15"])))
	// b[4] = x[1] ^ x[6] ^ x[7] ^ x[8] ^ x[12] ^ x[13] ^ x[14] ^ x[15]
	s.Assert(hmap["h4"].Eq((xmap["x1"]).Xor(xmap["x6"]).Xor(xmap["x7"]).Xor(xmap["x8"]).Xor(xmap["x12"]).Xor(xmap["x13"]).Xor(xmap["x14"]).Xor(xmap["x15"])))
	// b[5] = x[0] ^ x[4] ^ x[7] ^ x[8] ^ x[9] ^ x[10] ^ x[12] ^ x[13] ^ x[14] ^ x[15]
	s.Assert(hmap["h5"].Eq((xmap["x0"]).Xor(xmap["x4"]).Xor(xmap["x7"]).Xor(xmap["x8"]).Xor(xmap["x9"]).Xor(xmap["x10"]).Xor(xmap["x12"]).Xor(xmap["x13"]).Xor(xmap["x14"]).Xor(xmap["x15"])))
	// b[6] = x[1] ^ x[3] ^ x[7] ^ x[9] ^ x[10] ^ x[11] ^ x[12] ^ x[13] ^ x[15]
	s.Assert(hmap["h6"].Eq((xmap["x1"]).Xor(xmap["x3"]).Xor(xmap["x7"]).Xor(xmap["x9"]).Xor(xmap["x10"]).Xor(xmap["x11"]).Xor(xmap["x12"]).Xor(xmap["x13"]).Xor(xmap["x15"])))
	// b[7] = x[0] ^ x[1] ^ x[2] ^ x[3] ^ x[4] ^ x[8] ^ x[10] ^ x[11] ^ x[14]
	s.Assert(hmap["h7"].Eq((xmap["x0"]).Xor(xmap["x1"]).Xor(xmap["x2"]).Xor(xmap["x3"]).Xor(xmap["x4"]).Xor(xmap["x8"]).Xor(xmap["x10"]).Xor(xmap["x11"]).Xor(xmap["x14"])))
	// b[8] = x[1] ^ x[2] ^ x[3] ^ x[5] ^ x[9] ^ x[10] ^ x[11] ^ x[12]
	s.Assert(hmap["h8"].Eq((xmap["x1"]).Xor(xmap["x2"]).Xor(xmap["x3"]).Xor(xmap["x5"]).Xor(xmap["x9"]).Xor(xmap["x10"]).Xor(xmap["x11"]).Xor(xmap["x12"])))
	// b[9] = x[6] ^ x[7] ^ x[8] ^ x[10] ^ x[11] ^ x[12] ^ x[15]
	s.Assert(hmap["h9"].Eq((xmap["x6"]).Xor(xmap["x7"]).Xor(xmap["x8"]).Xor(xmap["x10"]).Xor(xmap["x11"]).Xor(xmap["x12"]).Xor(xmap["x15"])))
	// b[10] = x[0] ^ x[3] ^ x[4] ^ x[7] ^ x[8] ^ x[10] ^ x[11] ^ x[12] ^ x[13] ^ x[14] ^ x[15]
	s.Assert(hmap["h10"].Eq((xmap["x0"]).Xor(xmap["x3"]).Xor(xmap["x4"]).Xor(xmap["x7"]).Xor(xmap["x8"]).Xor(xmap["x10"]).Xor(xmap["x11"]).Xor(xmap["x12"]).Xor(xmap["x13"]).Xor(xmap["x14"]).Xor(xmap["x15"])))
	// b[11] = x[0] ^ x[2] ^ x[4] ^ x[6] ^ x[13]
	s.Assert(hmap["h11"].Eq((xmap["x0"]).Xor(xmap["x2"]).Xor(xmap["x4"]).Xor(xmap["x6"]).Xor(xmap["x13"])))
	// b[12] = x[0] ^ x[3] ^ x[6] ^ x[7] ^ x[10] ^ x[12] ^ x[15]
	s.Assert(hmap["h12"].Eq((xmap["x0"]).Xor(xmap["x3"]).Xor(xmap["x6"]).Xor(xmap["x7"]).Xor(xmap["x10"]).Xor(xmap["x12"]).Xor(xmap["x15"])))
	// b[13] = x[2] ^ x[3] ^ x[4] ^ x[5] ^ x[6] ^ x[7] ^ x[11] ^ x[12] ^ x[13] ^ x[14]
	s.Assert(hmap["h13"].Eq((xmap["x2"]).Xor(xmap["x3"]).Xor(xmap["x4"]).Xor(xmap["x5"]).Xor(xmap["x6"]).Xor(xmap["x7"]).Xor(xmap["x11"]).Xor(xmap["x12"]).Xor(xmap["x13"]).Xor(xmap["x14"])))
	// b[14] = x[1] ^ x[2] ^ x[3] ^ x[5] ^ x[7] ^ x[11] ^ x[13] ^ x[14] ^ x[15]
	s.Assert(hmap["h14"].Eq((xmap["x1"]).Xor(xmap["x2"]).Xor(xmap["x3"]).Xor(xmap["x5"]).Xor(xmap["x7"]).Xor(xmap["x11"]).Xor(xmap["x13"]).Xor(xmap["x14"]).Xor(xmap["x15"])))
	// b[15] = x[1] ^ x[3] ^ x[5] ^ x[9] ^ x[10] ^ x[11] ^ x[13] ^ x[15]
	s.Assert(hmap["h15"].Eq((xmap["x1"]).Xor(xmap["x3"]).Xor(xmap["x5"]).Xor(xmap["x9"]).Xor(xmap["x10"]).Xor(xmap["x11"]).Xor(xmap["x13"]).Xor(xmap["x15"])))

	sat, err := s.Check()
	if err != nil {
		fmt.Println("Failed non sat")
		os.Exit(1)
	}
	if sat {
		fmt.Println("Solution found")
		solution := make(map[string]z3.BV)
		model := s.Model()
		var solutionByChar z3.Int
		for k, v := range xmap {
			solution[k] = model.Eval(v, false).(z3.BV)
			solutionByChar = solution[k].SToInt()
			fmt.Printf("%s - %s\n", k, solutionByChar.String())
		}
	} else {
		fmt.Println("Solution not found")
	}

}
