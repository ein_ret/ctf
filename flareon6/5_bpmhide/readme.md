# Context

## Introduction

This is the walkthougth to 5-bmphide of Flareon6 challenge

# Tools

* Ghidra
* Dnspy

# Static analysis

The binary imports mscoree.dll which is library required by .NET application, we will used Dnspy to analyse this binary

## Overview

The binary took 3 arguments 
```
// BMPHIDE.Program
// Token: 0x06000018 RID: 24 RVA: 0x00002C18 File Offset: 0x00002C18
private static void Main(string[] args)
{
	Program.Init();
	Program.yy += 18;
	string filename = args[2];
	string fullPath = Path.GetFullPath(args[0]);
	string fullPath2 = Path.GetFullPath(args[1]);
	byte[] data = File.ReadAllBytes(fullPath2);
	Bitmap bitmap = new Bitmap(fullPath);
	byte[] data2 = Program.h(data);
	Program.i(bitmap, data2);
	bitmap.Save(filename);
}
```

* The secret is processing by the h() functions
	* f(), e(), a(), c() functions are used
* The image is written by using the i() function
	* j() function is used
* b(), d(), g()	seems to be unused

we suppose that :

* 1st argument is an input image
* 2nd argument is a secret file
* 3nd argument is the output image

if we run the program with with dnspy, it crashed by throwing a stackOverflowException. The capture below show the function stack trace.

![crash2](png/crash2.PNG)

![crash](png/crash.PNG)
PNG
# Analyse why does it crash ?

By digging in the previous functions we saw that there is complex stuff happening dealing with LoadLibrary, GetDelegateForFunctionPointer, virtualProtect... below a capture from A.IdentifyLocals() function

![identifyLocal](png/identify_local.PNG)

VirtualProtect function is used to change the memory permission :

```
BOOL VirtualProtect(
  LPVOID lpAddress,
  SIZE_T dwSize,
  DWORD  flNewProtect,
  PDWORD lpflOldProtect
);
```

* PAGE_EXECUTE_READWRITE 0x40 = 64 has been set as permission
* The next instructions `Marshal.WriteIntPtr(intPtr2, 2, val)` write val at *intPtr2+1 (byte). We can assume that there is some dynamic code changing in the runtime that cause the crash when opening the debugger.
* The `IncrementalMaxStack` function compares the `methodBase.MetadataToken` to some constant 100663317 = 0x6000015 and 100663316 = 0x6000014, then changing ILcode. I guess that in the runtime, another method is called.


# Conclusion

Unfortunately, I don't success to clearly understand how I can avoid program crashing when debugging nor resolve this challenge, however I learn a lot during this exercise with obfuscation technique at runtime.
