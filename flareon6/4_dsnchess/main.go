package main

import (
	"fmt"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

type simpleIP []int

func convertIPStringToIP(allIPString []string) []simpleIP {
	ips := make([]simpleIP, 0)
	for _, ipString := range allIPString {
		tmpIntArray := make([]int, 4) // IP representation ex: [127,0,0,1]
		ipStringArray := strings.Split(ipString, ".")
		for i, v := range ipStringArray {
			intConversion, err := strconv.Atoi(v)
			if err != nil {
				log.Fatalf("failed converting IP to string %v", err)
			}
			tmpIntArray[i] = intConversion
		}
		ips = append(ips, tmpIntArray)
	}
	return ips
}

func main() {
	// Declare variables

	// cipher is extracted from Ghidra at 0x00102020
	cipher := []int{0x79, 0x5a, 0xb8, 0xbc, 0xec, 0xd3, 0xdf, 0xdd, 0x99, 0xa5, 0xb6, 0xac, 0x15, 0x36, 0x85, 0x8d, 0x09, 0x08, 0x77, 0x52, 0x4d, 0x71, 0x54, 0x7d, 0xa7, 0xa7, 0x08, 0x16, 0xfd, 0xd7}
	// ipsString is extracted from pcap file
	ipsString := []string{"127.150.96.223", "127.252.212.90", "127.215.177.38", "127.118.118.207", "127.89.38.84", "127.109.155.97", "127.217.37.102", "127.49.59.14", "127.182.147.24", "127.0.143.11", "127.227.42.139", "127.101.64.243", "127.201.85.103", "127.200.76.108", "127.50.67.23", "127.157.96.119", "127.99.253.122", "127.25.74.92", "127.168.171.31", "127.148.37.223", "127.108.24.10", "127.37.251.13", "127.34.217.88", "127.57.238.51", "127.196.103.147", "127.141.14.174", "127.238.7.163", "127.230.231.104", "127.55.220.79", "127.184.171.45", "127.196.146.199", "127.191.78.251", "127.159.162.42", "127.184.48.79", "127.127.29.123", "127.191.34.35", "127.5.22.189", "127.233.141.55", "127.55.250.81", "127.53.176.56"}
	maximalRound := 15
	flag := make([]int, len(cipher))

	// Find flag
	ips := convertIPStringToIP(ipsString)
	for round := 0; round < maximalRound; round++ {
		for _, ip := range ips {
			if ip[0] != 127 || (ip[3]&1) != 0 || round != int(ip[2])&0xf {
				continue
			}
			flag[round*2] = cipher[round*2] ^ ip[1]
			flag[round*2+1] = cipher[round*2+1] ^ ip[1]
		}
	}
	fmt.Printf("flag is : %c\n", flag)
}