# Context

## Introduction

This is the walkthougth to 4-dnschess of Flareon6 challenge

# Tools

* wireshark
* tshark
* ghydra

# Static analysis

Two main files one is network capture file and an ELF binary with .so dynamic library :

## Pcap file 

* The pcap is composed of dns record with request name is a chess move

![chess](png/dns.PNG)

Lets extract the dns request name by using tshark

```
tshark -r capture.pcap -Y "dns.flags.response == 1" -T fields -e dns.a -e dns.qry.name | sed 's/,127.0.0.1//'
127.150.96.223	rook-c3-c6.game-of-thrones.flare-on.com
127.252.212.90	knight-g1-f3.game-of-thrones.flare-on.com
127.215.177.38	pawn-c2-c4.game-of-thrones.flare-on.com
127.118.118.207	knight-c7-d5.game-of-thrones.flare-on.com
127.89.38.84	bishop-f1-e2.game-of-thrones.flare-on.com
127.109.155.97	rook-a1-g1.game-of-thrones.flare-on.com
127.217.37.102	bishop-c1-f4.game-of-thrones.flare-on.com
127.49.59.14	bishop-c6-a8.game-of-thrones.flare-on.com
127.182.147.24	pawn-e2-e4.game-of-thrones.flare-on.com
127.0.143.11	king-g1-h1.game-of-thrones.flare-on.com
127.227.42.139	knight-g1-h3.game-of-thrones.flare-on.com
127.101.64.243	king-e5-f5.game-of-thrones.flare-on.com
127.201.85.103	queen-d1-f3.game-of-thrones.flare-on.com
127.200.76.108	pawn-e5-e6.game-of-thrones.flare-on.com
127.50.67.23	king-c4-b3.game-of-thrones.flare-on.com
127.157.96.119	king-c1-b1.game-of-thrones.flare-on.com
127.99.253.122	queen-d1-h5.game-of-thrones.flare-on.com
127.25.74.92	bishop-f3-c6.game-of-thrones.flare-on.com
127.168.171.31	knight-d2-c4.game-of-thrones.flare-on.com
127.148.37.223	pawn-c6-c7.game-of-thrones.flare-on.com
127.108.24.10	bishop-f4-g3.game-of-thrones.flare-on.com
127.37.251.13	rook-d3-e3.game-of-thrones.flare-on.com
127.34.217.88	pawn-e4-e5.game-of-thrones.flare-on.com
127.57.238.51	queen-a8-g2.game-of-thrones.flare-on.com
127.196.103.147	queen-a3-b4.game-of-thrones.flare-on.com
127.141.14.174	queen-h5-f7.game-of-thrones.flare-on.com
127.238.7.163	pawn-h4-h5.game-of-thrones.flare-on.com
127.230.231.104	bishop-e2-f3.game-of-thrones.flare-on.com
127.55.220.79	pawn-g2-g3.game-of-thrones.flare-on.com
127.184.171.45	knight-h8-g6.game-of-thrones.flare-on.com
127.196.146.199	bishop-b3-f7.game-of-thrones.flare-on.com
127.191.78.251	queen-d1-d6.game-of-thrones.flare-on.com
127.159.162.42	knight-b1-c3.game-of-thrones.flare-on.com
127.184.48.79	bishop-f1-d3.game-of-thrones.flare-on.com
127.127.29.123	rook-b4-h4.game-of-thrones.flare-on.com
127.191.34.35	bishop-c1-a3.game-of-thrones.flare-on.com
127.5.22.189	bishop-e8-b5.game-of-thrones.flare-on.com
127.233.141.55	rook-f2-f3.game-of-thrones.flare-on.com
127.55.250.81	pawn-a2-a4.game-of-thrones.flare-on.com
127.53.176.56	pawn-d2-d4.game-of-thrones.flare-on.com
```

## Binary

By opening the ELF binary we saw that all the logic is handle by the .so library :

![elf](png/elf.PNG)

The method getNextMove looks interesting, we find the code in the .so with some comments added :

![gnm](png/gnm.PNG)

we saw that a DNS request is performed via `phVar2 = gethostbyname(user_move);` and some check are performed based on the ip of the requested domain.

In order to retrieve the flag we need to :

* Use the cipher located at 0x102020 `79 5a b8 bc ec d3 df dd 99 a5 b6 ac 15 36 85 8d 09 08 77 52 4d 71 54 7d a7 a7 08 16 fd d7`
* Try all the ip got in the pcap file and check to :
```
  if ((((hostent == (hostent *)0x0) || (pcVar1 = *hostent->h_addr_list, *pcVar1 != '\x7f')) ||
      ((pcVar1[3] & 1U) != 0)) || (round_number != ((uint)(byte)pcVar1[2] & 0xf))) {
    returncode = 2;
  }
  else {
    sleep(1);
    (&ptrFlag)[round_number * 2] = (&ptrCipher)[round_number * 2] ^ pcVar1[1];
    (&ptrFlag)[round_number * 2 + 1] = (&ptrCipher)[round_number * 2 + 1] ^ pcVar1[1];
```

The code in `main.go` file allows us to get the flag :`## flag is : [L o o k s L i k e Y o u L o c k e d U p T h e L o o k u p Z]` _(without [] and space)_