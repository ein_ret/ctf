# Context

## Introduction

This is the walkthougth to 1-MememCatBattlestation of Flareon6 challenge

## Tools

* Ghidra

## Static analysis

The binary is in .NET and divided into 2 stages :

* The first stage must be equal to RAINBOW

![stage1](png/stage1.PNG)

* The second stage take our string and xor it with "A" :

![stage2](png/stage2.PNG)

We need to xor the from itertools import cycle with "A", the following python script do it :

* Golang
```
package main

import "fmt"

func main() {
    cipher:=[]byte("\u0003 &$-\u001e\u0002 //./")
    plaintext:=make([]byte,len(cipher))
    for i,v := range cipher{
        plaintext[i]=v^65 //65 is A
    }
    fmt.Printf("Solution %s\n",plaintext)
}
```

* Python
```
from itertools import cycle
cipher="\u0003 &$-\u001e\u0002 //./"                                                            
print("".join([chr(ord(single_s)^ord(key)) for single_s,key in zip(cipher,cycle("A"))]))

Bagel_Cannon
```

![solution](png/solution.PNG)
