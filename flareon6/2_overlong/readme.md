# Context

## Introduction

This is the walkthougth to 2-overlong of Flareon6 challenge

# Tools

* Ghidra

# Static analysis

The binary is very small and the entry point is the following :

![entry](png/entry.PNG)

The messageBox displays the local_88 content which is zeroed at the offset local_8. The FUN_00401160 makes some string transformation with the content located at &DAT_00402008. 

By changing the data format to string in Ghydra at the location &DAT_00402008 we get the flag

![solution](png/solution.PNG)